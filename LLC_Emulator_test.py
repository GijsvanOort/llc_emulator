# LLC_Emulator_test.py
import threading
import socket
import time

## Parameters
#IP='130.89.64.89'
IP='10.30.203.124'
#IP='127.0.0.1'

## Class definitions
# A very simple script to test the response of the LLC_emulator.py.

class ReceiveChannel(threading.Thread):
    def __init__(self, recvPort, delay=0.1, debugLevel = 2):
        """Initializes (threading on background) recvPort.
        
        debugLevel 0: Just prints received messages
        debugLevel 1: prints warnings and errors
        # debugLevel 2: also prints received and transmitted messages
        debugLevel 3: also prints each poll, even if no message was received/transmitted.
        """
        threading.Thread.__init__(self)
        self.setName('ReceiveChannel_%d'%(recvPort))
        
        self.recvPort = recvPort
        self.debugLevel = debugLevel
        self.delay = delay

        self.recvSock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
        self.recvSock.bind( ('',self.recvPort) )
        self.recvSock.setblocking(False)
        self.t0 = time.time()
        
    def Poll(self):
        """Does the real work: checks if something is waiting on this port, and
        if so, it returns data to the send port."""
        
        # Read, if possible
        try:
            data,addr = self.recvSock.recvfrom( 1600 )
        except socket.error as T:
            if (T.errno == 10035):
                # No incoming data
                if (self.debugLevel>=3):
                    print ("No incoming data on port %d."%self.recvPort)
                return
            else:
                raise # reraise error
        # If we're here, there is data.
        if (len(data)>1450 and self.debugLevel>=1):
            print ("Warning: length of incoming data on port %d is more than 1450 bytes!"% self.recvPort)
        
        print ("%.2f   Received on port %5d (length: %4d): %s"%(time.time()-self.t0, self.recvPort, len(data), data.decode()))
        # Done.
        
    def run(self):
        """Threading function"""
        self._stopbool = False
        while (not self._stopbool):
            self.Poll()
            time.sleep(self.delay)
            
        # Allow for restart (basically re-initializes the thread class)
        threading.Thread.__init__(self)
            
    def stop(self):
        """Makes the running thread stop."""
        self._stopbool = True
            
    def SetDebugLevel(self,n):
        self.debugLevel = n

    def GetDebugLevel(self):
        return self.debugLevel
        
    def __del__(self):
        """Destructor"""
        self.recvSock.close()
        
class SendChannel:
    def __init__(self, sendIp, sendPort, debugLevel = 2):
        """Initializes. sendPort 
        
        debugLevel 0: no printing at all
        debugLevel 1: prints warnings and errors
        debugLevel 2: also prints received and transmitted messages
        debugLevel 3: also prints each poll, even if no message was received/transmitted.
        """
        self.sendIp = sendIp
        self.sendPort = sendPort
        self.debugLevel = debugLevel
        
        self.sendSock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
        self.t0 = time.time()

    def Send(self,s):
        """Sends something through the port."""
                
        self.sendSock.sendto(s.encode(),(self.sendIp,self.sendPort))
        if (self.debugLevel>=2):
            print ("%.2f Sent     to port %5d (length: %4d): %s"%(time.time()-self.t0, self.sendPort, len(s), s))
        # Done.
        
    def SetDebugLevel(self,n):
        self.debugLevel = n

    def GetDebugLevel(self):
        return self.debugLevel
        
    def __del__(self):
        """Destructor"""
        self.sendSock.close()
        
class LLC_Emulator_test:
    def __init__(self):
        self._x1Send = SendChannel(IP,25000)
        self._x1Recv = ReceiveChannel(25001,debugLevel=2,delay=0.1)
        self._x1Recv.start()
        
    def __del__(self):
        self._x1Recv.stop()
        del self._x1Recv
        del self._x1Send
    
    def SendCommand(self,s,delay=0.2):
        """Sends command and waits a while to receive the answer"""
        print("   ",end='')
        self._x1Send.Send(s)
        time.sleep(delay)
        print("")
        #input("press key...")
            
    def Menu(self):
        """Continuously asks for data to send over the (only=first) port. Quit by typing 'q' (without quotes)"""
        print ("Type something to send, q for quit\n----------------------------------");
        while True:
            s = input("<> ")
            if (s=='q'):
                print("\nQuitting")
                break
            elif (len(s)>0):
                self._x1Send.Send(s)
                time.sleep(0.3);
            else:
                print("")

        
    def SendStandardCommandSet(self):
        """Sends a series of commands"""
        s = self.SendCommand
        print("================")
        print("IP: "+IP)
        print("================")
        s('resetall')
        
        print("\nState transitions")
        s('set state off')
        s('get state')
        s('set state init')
        t0 = time.time()
        for x in range(16):
            print('t=%.1f'%(time.time()-t0))
            s('get state', delay=1.0)
        
        s('set state force')
        s('get state')
        
        print ("\nExamples from the documentation")
        s('set state off')
        s('get state')
        s('get modelvelpatient')
        s('create patientspring myspring')
        s('set myspring stiffness [0,0,0,0,0,10,0,0,0,0]')
        s('get modelpospatient')
        s('get measpospatient')
        s('set treadmill 1.5')
        s('get workspacepatient')
        s('get larie')
        s('set treadmill "apekool"')
        s('set state init')
        s('set state force')
        
        s('create patientspring testspring')
        
        print("\nError-generating commands")
        s('set inertia [1,2,3]')
        s('set inertia [1e-4,2,3,4,5,6,7,8,9,10')
        s('set inertia [,,,,55,,,,,]')
        s('set inertia [1,2,3,4,false,true,7,8,9,10]')
        s('set inertia 1,2,3,4,5,6,7,8,9,10')
        s('set blabdiebla')
        s('get bladiebla')
        s('set measpospatient [1,2,3,4]')
        s('set treadmill [5.5]')
        s('set isInitOk True')
        s('set isinitok blablabla')
        s('set isforcecalibrated false')
        s('set state bladiebla')
        s('set testspring enabled true')
        s('create patientspring testspring')
        s('set testspring bladiebla 22')
        s('set watchdogtimeout -1.000')
        

        print("\nExtra arguments are ignored; comma is string separator:")
        s('set inertia [1,2,3,4,5,6,7,8,9,10] en nog wat')
        s('get inertia blabla')
        s('set msgid hoi dag')
        s('set msgid hoi,dag')
        
        print ("\nNormal commands")
        s('set inertia [1,2,3,4,5,6,7,8,9,10]')
        s('get inertia')
        s('get measPosPatient')
        s('get treadmill')
        s('set treadmill 2.0')
        s('get patientDim')
        s('set watchdogtimeout 1.0001')
        s('get WatchDogTimeOut')
        s('set msgid hoi')
        s('set msgid 123')
        s('set isinitok false')
        s('calibratePatient')
        s('calibrateForceSensor')
        s('calibrateForceSensors')
        s('resettreadmilldistance')
        s('remove all')
        s('get istreadmillconsistent')
        s('get isemergencybuttonreleased')
        
        print("\nEffect handling")
        s('create patientSpring abcdefgh')
        s('set abcdefgh pos [1,2,3,4,5,6,7,8,9,10]')
        s('set abcdefgh vel [1,2,3,4,5,6,7,8,9,10]')
        s('set abcdefgh maxforce [1,2,3,4,5,6,7,8,9,10]')
        
        s('set abcdefgh stiffness [1,2,3,4,5,6,7,8,9,10]')
        s('set abcdefgh dampfactor [1,2,3,4,5,6,7,8,9,10]')
        s('set abcdefgh halfdeadband [1,2,3,4,5,6,7,8,9,10]')
        s('set abcdefgh dampcoef [1,2,3,4,5,6,7,8,9,10]')
        
        
        
        s('get abcdefgh vel')
        s('set abcdefgh enable')
        s('set abcdefgh disable')
        s('get abcdefgh maxforce')
        s('get abcdefgh dampfactor')
        s('remove abcdefgh')
 
      
 
        
        
        
s=input("Menu (m) or standard command set test (s)? ")
print("")
s=str.lower(s)
if (s[0] == 'm'):
    A = LLC_Emulator_test()
    A.Menu()
elif (s[0]=='s'):
    A = LLC_Emulator_test()
    A.SendStandardCommandSet()
else:
    print("Wrong choice, quitting")
            
            
 