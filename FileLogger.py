# Logger
import datetime

class FileLogger:
    def __init__(self,fileName):
        """Use a filename without extension (.csv is added here). Also, a timestamp is added to the filename"""
        self._fileName = fileName+datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M')+'.csv'
        self._f = open(self._fileName,'w')
        self._headerPrinted = False
        
    def _header(self,hdr):
        self._f.write(','.join([str(x) for x in hdr])+'\n')
        
    def Log(self,l,hdr):
        """l and hdr should be a same-length list (hdr be a string list)"""
        if not self._headerPrinted:
            self._header(hdr)
            self._headerPrinted = True
            
        self._f.write(','.join([str(x) for x in l])+'\n')
        
        
    def __del__(self):
        self._f.close()