d:
cd D:\Gijs\Lopes\LLC_Emulator
c:\gijssoftwarenoinstall\pyzo2013c\python LLC_Emulator.py



@echo off
rem Wait for 60 seconds before closing the cmd window. In this 60 seconds, 
rem the user has the option to cancel automatic closing (for example, to examine
rem the error messages
echo.
echo.
echo Press K to keep this window open, C to close immediately.
choice /T 60 /C kc /D C /N /M "You have 60 seconds before the window is automatically closed."
set E=%errorlevel%
if %E%==1 (
  echo. 
  choice /C cqx /N /M "Keeping this window open until you press C."
)
rem Close immediately if choice was not 1.