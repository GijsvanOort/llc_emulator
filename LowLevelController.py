## LLC Emulator
#
# This script acts as if it is the LLC; it sends commands like the LLC will
# do in the future.

from PySide import QtCore# import QThread, QObject,QMutex
#from PySide import *
import socket
import time
import re
import random
import math
import numpy as np
import visvis as vv
from pprint import pprint as pp
from HumanWalkModel import UseHumanWalkModel
from PerReader import PerReader

dataMutex = QtCore.QMutex()

## Parameters

## Helper functions

        
def lenn(x):
    """Advanced len. Returns:
       - the length of x if it is a list or tuple;
       - 1 if it is an integer, float, string (!!) or bool
       - 0 if it x is None
       - raises NotImplementedError otherwise
    """
    T = type(x)
    if (T in (list, tuple)): return len(x)
    elif (T in (str,int,float,bool,np.float64)): return 1
    elif x is None: return 0
    else:
        raise NotImplementedError('Variable type %s not implemented in lenn'%T.__name__)
    
def sumel(a,b):
    """Given lists a and b of the same length, it returns the vector-like sum.
    If the lists are not of the same length, we get an exception"""
    if len(a) != len(b):
        raise ArithmeticError('vectors a and b not same length')
    return [x[0]+x[1] for x in zip(a,b)]

def diffel(a,b):
    """Given lists a and b of the same length, it returns the vector-like difference a-b.
    If the lists are not of the same length, we get an exception"""
    if len(a) != len(b):
        raise ArithmeticError('vectors a and b not same length')
    return [x[0]-x[1] for x in zip(a,b)]

def prodel(a,b):
    """Given lists a and b of the same length, it returns the element-wise product.
    If the lists are not of the same length, we get an exception"""
    if len(a) != len(b):
        raise ArithmeticError('vectors a and b not same length')
    return [x[0]*x[1] for x in zip(a,b)]

def prodsc(vec,x):
    """Returns 'vector' vec multiplied by scalar x"""
    return [z*x for z in vec]

def divel(a,b):
    """Given lists a and b of the same length, it returns the element-wise quotient a/b.
    If the lists are not of the same length, we get an exception"""
    if len(a) != len(b):
        raise ArithmeticError('vectors a and b not same length')
    return [x[0]/x[1] for x in zip(a,b)]
    
def limitel(a,minn,maxx):
    if len(a) != len(minn) or len(a) != len(maxx):
        raise ArithmeticError('vectors not same length')
    return [ max(min(x,mx),mn) for x,mn,mx in zip(a,minn,maxx)]
    
def neg(a):
    """Returns a list with each element of a negated"""
    return [-x for x in a]
    
def pr(a,pre=""):
    """Pretty prints a list"""
    print (pre+" ["+", ".join(["%6.3f"%x for x in a])+"]")
    
def accForParabolicTrajectory( x_now, v_now, x_des, v_max, a_max, tolerance=0.001, vtolerance=0.001):
    """Returns an appropriate acceleration for bringing x to x_des.
    Also works for arrays (in that case, all parameters should be a list of identical length).
    as a second return argument, it returns a boolean 'Done'.
    """
    if isinstance(x_now,list):
        if not isinstance(tolerance,list):
            tolerance = len(x_now)*[tolerance]
        if not isinstance(vtolerance,list):
            vtolerance = len(x_now)*[vtolerance]
        yy = [ accForParabolicTrajectory(a,b,c,d,e,f,g) for a,b,c,d,e,f,g in zip(x_now, v_now, x_des, v_max, a_max, tolerance,vtolerance)]
        a = [x[0] for x in yy]
        done = all([x[1] for x in yy])
        return a,done
        
    if abs(x_now-x_des) < tolerance and abs(v_now) < 10*vtolerance:
        if abs(v_now)<vtolerance:
            return -10*v_now, True
        else:
            return -10*v_now, False
    if v_now >= 0:
        # Check where we end up (i.e., what is x when v=0) when from
        # now we do maximum deceleration. If it 's below x_des, we need to
        # accelerate, otherwise we need to decelerate
        # Equations: v = -a_max * t + v_now; x = 0.5 * -a_max * t^2 + v_now * t + x_now
        # Solve for (x,t) with v=0
        t = v_now / a_max
        x = 0.5 * -a_max * t**2 + v_now * t + x_now # x is now the position when v=0
        
        if abs(x-x_des) < tolerance:
            return 0, False
        elif x<x_des:
            if v_now>=v_max:
                return 0, False
            else:
                return a_max, False
        else:
            return -a_max, False
    else: # v_now<0:
        a,done = accForParabolicTrajectory( -x_now, -v_now, -x_des,v_max,a_max, tolerance, vtolerance)
        return -a, done
    
# Alias for print, so that we can override it easily for everything that needs to be written to the GUI
my_print = print
debug_print = print


## Lopes-specific helper functions
def SegmentPositionsToJointPositions(x):
    """Returns an array of 10 elements with joint positions, given another array of 10 elements
    with segment positions. Also works for converting velocities and accelerations.
    Note that these are _not_ my own anatomical values (the signs are different) but the
    values as defined in LLC API 2.6."""
    return [x[0],x[1],x[2],x[3],x[4]-x[3], x[5]-x[4],x[6],x[7],x[8]-x[7],x[9]-x[8]]

def JointForcesToSegmentForces(f):
    """Returns an array of 10 elements with segment forces, given another array of 10 elements
    with joint forces. Note that these are _not_ my own anatomical values (the signs are different) but the
    values as defined in LLC API 2.6."""
    return [f[0],f[1],f[2],f[3]-f[4],f[4]-f[5],f[5],f[6],f[7]-f[8],f[8]-f[9],f[9]]
    
def AnatomicPositionsToJointPositions(x):
    """Given an array of 10 elmements with joint positions (PX, PZ, LHA, LHF, LKF, LAP, RHA, RHF, RKF, RAP),
    with positive directions defined as rotating in the direction indicated by the abbreviation), it
    returns an array of JointPositions as expressed in LLC API 2.6
    (i.e., positive is cw when looking into the positive direction of the axis).
    Note that this function can also be used to do the conversion backwards (i.e., it is its own inverse)."""
    return [ x[0], x[1], x[2], x[3], -x[4], -x[5], -x[6], x[7], -x[8], -x[9] ]
JointPositionsToAnatomicPositions = AnatomicPositionsToJointPositions # Alias for the backward conversion

def CalculateDamping(stiffness, relativeDamping, mass):
    """Returns a damping term (in Ns/m or Nms/rad) based on the inputs. Inputs should be lists (of same length)."""
    return [  zeta*2*math.sqrt(m*abs(k)) for k,zeta,m in zip(stiffness, relativeDamping, mass) ]

def PatientInertia(Mass,Length):
    """Returns a list of inertias for the patient. This is quite approximate, based
    on 'Physics of the human body', by Irwing P. Herman, 
    books.google.nl/books?isbn=3540296042"""
    # Data from book: [wholeLeg, upper leg, lower leg, foot], pages 17 and on
    relMass   = np.array([0.161, 0.100, 0.0465, 0.0145])
    relLength = np.array([0.530, 0.245, 0.246,  0.152])
    relRadGyr = np.array([0.560, 0.540, 0.528,  0.690]) # Radius of gyration relative to segment length, when rotating around proximal joint

    # Calculate everything
    massSegments = Mass*relMass
    lengthSegments = Length*relLength
    radGyrSegments = lengthSegments*relRadGyr # elementwise multiplication
    I_proximal = massSegments*radGyrSegments*radGyrSegments # kgm^2
    return [ Mass, Mass ] + 2*list(I_proximal)
    
## Class definitions of UDP communication channel/error channel
class ParseError(Exception):
    pass

class ErrorChannel(QtCore.QObject):
    ERROR_PRE='--- ERROR: '
    debugPrintSignal = QtCore.Signal(str) 
    def __init__(self, sendIP, sendPort, llcClass, debugLevel = 2):
        """Initializes an error channel. This is much simpler than the
        communication channel because this one can only send and therefore does not
        need to be able to poll etc.
        
        debugLevel 0: no printing at all
        debugLevel 1: prints warnings and errors
        debugLevel 2: also prints received and transmitted messages
        debugLevel 3: also prints each poll, even if no message was received/transmitted.
        """
        QtCore.QObject.__init__(self)
        
        self.sendIP = sendIP
        self.sendPort = sendPort
        self.debugLevel = debugLevel
        self.llcClass = llcClass

        self.sendSock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
        self.nMessagesSent = 0 # Number of messages sent (is also number of messages received)
        
    def TrueFalseString(self,b):
        return '"true"' if b else '"false"'
        
  
    def Send(self,s):
        """Sends string s to the port. The string is prepended with "---Error: ".
        """
        s = self.ERROR_PRE+s
        if self.sendIP is not None:
            sBytes = s.encode()
            if (len(sBytes)>1450 and self.debugLevel>=1):
                my_print ("Warning: length of outgoing data on port %d is more than 1450 bytes!"%self.sendPort)
            
            self.sendSock.sendto(s.encode(), (self.sendIP, self.sendPort))
            self.nMessagesSent += 1
            if (self.debugLevel>=2):
                self.debugPrintSignal.emit("Sent     to port %5d (length: %4d): *%s*"%(self.sendPort, len(sBytes),s))
                self.debugPrintSignal.emit("")
        else:
            self.debugPrintSignal.emit("Error: No HLC IP given; could not send message through Error Channel.")
        
    def SetHLCIp(self,s):
        self.sendIP = s
        
    def GetHLCIp(self):
        return self.sendIP
        
    def SetDebugLevel(self,n):
        self.debugLevel = n

    def GetDebugLevel(self):
        return self.debugLevel
        
    def __del__(self):
        """Destructor"""
        self.sendSock.close()
        

class CommunicationChannel(QtCore.QThread):
    
    debugPrintSignal = QtCore.Signal(str) 
    def __init__(self, recvPort, sendPort, llcClass, delay=0.01, debugLevel = 2):
        """Initializes. recv/sendPort are the ports from which this program
        should receive data and transmit answers. Also intiializes the threading,
        so that you can run  this as a thread. As a thread, it will execute Poll
        with intermediate delays of delay.
        
        debugLevel 0: no printing at all
        debugLevel 1: prints warnings and errors
        debugLevel 2: also prints received and transmitted messages
        debugLevel 3: also prints each poll, even if no message was received/transmitted.
        """
        QtCore.QThread.__init__(self)
        self.setObjectName('CommChannel_%d_%d'%(recvPort, sendPort))
        
        self.recvPort = recvPort
        self.sendPort = sendPort
        self.debugLevel = debugLevel
        self.delay = delay
        self.llcClass = llcClass

        self.recvSock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
        self.recvSock.bind( ('',self.recvPort) )
        self.recvSock.setblocking(False)
        
        self.sendSock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
        self.nMessagesSent = 0 # Number of messages sent (is also number of messages received)
        self.isOn=False
    
    def TrueFalseString(self,b):
        return '"true"' if b else '"false"'
        
    def str2values(self, s, alwaysAsList = False):
        """Given a string, it tries smartly to parse the values. The following are possible:
        - A vector in brackets: [1.2, True, 2.3] returns a list of these values (each
          recursively evaluated individually by this function
        - Anything which is not a vector can be either:
          - Anything which can be parsed as a float is done as such (e.g., '-45e-2' -> -0.45)
        - the words true and false (case-insensitive) are recognised and returned as a bool
        - If the string could not be parsed, the string itself is returned.
        Any leading/trailing white spaces are trimmed first, all strings are converte
        to lower case.
        If alwaysAsList is True, the result is returned as a list, even if it is only
        one element long
        - If there is only an opening or closing bracket, 
        
        Examples:
        str2values("[1.5, tRue, bla bla]") # -> [1.5, True, 'bla bla']
        str2values("[3,4,-4e-2];")    # -> [3.0, 4.0, -0.04]
        str2values("41.999")          # -> 41.9999
        str2values("41.999", True)    # -> [41.9999]
        str2values("foo")             # -> 'foo'
        str2values("[2,3,4")          # -> '[2,3,4' (no closing bracket; not recognized as vector)
        """
    
        s = s.strip()
        s = str.lower(s)
        if (len(s)==0):
            y = ''
        
        elif (s[0]=='[' and s[-1] == ']'):
                # Check if there is a vector
                y = [self.str2values(z) for z in s[1:-1].split(',')]
        elif (s == 'true'):
            y = True
        elif (s == 'false'):
            y = False
        else:
            # Try to parse it as a float
            try:
                y = float(s)
            except ValueError as V:
                y = s
        if (type(y) is not list and alwaysAsList):
            y = [y]
        return y
        
    def SplitCommand(self,s):
        """Returns the command, split in tokens. Spaces separate tokens, except
        in a vector, where commas separate them. True, and False (case insensitive)
        are evaluated to a bool. All strings are converted to lower case.
        Vectors can not be nested (if so, this function gives undefined results).
        If there is just an opening or closing bracket, (not both) the result of this
        function is undefined. As a second argument, it returns raw splitted values (i.e., all strings)
        Examples:
        p=print    
        p(SplitCommand('set treadmill 4.3')) # ['set', 'treadmill', 4.3]
        p(SplitCommand('get   isPatientDefined')) # ['get', 'ispatientdefined']
        p(SplitCommand('set isInitOk false')) # ['set', 'isinitok', False]
        p(SplitCommand('set inertia [1,2, 3.0,  -4,  5e-2, 6])) # ['set', 'inertia',[1.0,2.0,3.0,-4.0,0.05,6]]
        p(SplitCommand('set something[1,2]')) # ['set', 'something', [1.0,2.0]]
        """
        # First step: split tokens (a vector is one token)
        # Matches: 
        # - any group of characters contining only non-spaces and '[' and ']'
        # - an opening bracket followed by zero or more non-brackets followed by a closing bracket.
        expr = '[^\[\] ]+|\[[^\]]*\]'
        ss = re.findall(expr,s)
        
        # Second step: parse tokens individually; we use str2values for this
        return [ self.str2values(x) for x in ss], ss
        
        
        
    def ParseCommand(self, s):
        """Parses one command, returns a reply. The commands in the LLC are case-
        insensitive, so I will use lower case here for everything."""
        def GetEl(l, i, default):
            """Returns the i'th element of l if it exists, otherwise it returns default"""
            return l[i] if i<len(l) else default
        
        ERROR_PRE='--- ERROR: '
        Q = '"' # a quote
        
        
        s = s.strip().lower()
        if len(s)==0:
            return (Q+ERROR_PRE+"No command given"+Q) # Empty string was given
        if s[-1]=='\x00':
            s=s[0:-1]
        sp,sp_raw = self.SplitCommand(s)
        nTokens= len(sp)
        sp = sp+['']*10; sp_raw = sp_raw+['']*10
        # Now sp is very long, so any sp[...] exists. It is '' if originally it did not exist.
        
        if (nTokens==0): return ''# No command -> no return value
        
        if (sp[0] == 'set'):
            if (sp[1] == 'msgid'): 
                i=sp_raw[2].find(',')
                if (i!=-1): sp_raw[2] = sp_raw[2][0:i]
                return Q+('msgID=%s'%sp_raw[2])+Q # Just reply the same thing
            elif (sp[1] == 'state'): 
                try:
                    return self.llcClass.RequestStateChange(sp[2])
                except ParseError as L:
                    return (Q+ERROR_PRE+L.__str__()+Q)
            elif (sp[1] in ['inertia','coulombfriction','workspacepatient',
                    'treadmill','accelerationref','accelerationrefgain','patientdim','watchdogtimeout']):
                try:
                    return self.llcClass.SetProperty(sp[1],sp[2])
                except ParseError as L:
                    return (Q+ERROR_PRE+L.__str__()+Q)
            elif (sp[1] in ['isinitok','ispatientdefined','ispatientcalibrated']):
                # Booleans can only be set to false; not to true. If we try something
                # different, it will be noticed in SetStatusBoolean()
                try:
                    return self.llcClass.SetStatusBoolean(sp[1],sp[2])
                except ParseError as L:
                    return (Q+ERROR_PRE+L.__str__()+Q)
                
            else:
                # sp[1] is an effect name
                if (sp[2] == 'enable'):
                    try:
                        self.llcClass.SetEffectProperty(sp[1], 'enable',None)
                        return ('"Effect enabled"')
                    except ParseError as L:
                        return (Q+ERROR_PRE+L.__str__()+Q)
                elif (sp[2] == 'disable'):
                    try:
                        self.llcClass.SetEffectProperty(sp[1], 'disable',None)
                        return ('"Effect disabled"')
                    except ParseError as L:
                        return (Q+ERROR_PRE+L.__str__()+Q)
                else:
                    # A normal property: sp[2] is property name, sp[3] is value
                    def NoneString(a): return a if a is not None else '##None##'
                    
                    try:
                        r = self.llcClass.SetEffectProperty(sp[1], sp[2], sp[3])
                        
                        return Q+NoneString(r)+Q
                    except ParseError as L:
                        return Q+ERROR_PRE+L.__str__()+Q
                
        elif (sp[0] == 'resetall'):
            self.llcClass.Reset()
            return ('"LOPES reset successfully"')
        elif (sp[0] == 'resetdrives'):
            return ('"All MOOG Servo Drives reset"')    
        elif (sp[0]=='get'):
            if (sp[1] in ['segmentmeasvel', 'segmentmeasacc']):
                # These don't exitst in the LLC, but they are measurements inside this emulator. This clause
                # forces an error message to be shown (instead of returning the values from the measurements)
                return (Q+ERROR_PRE+("Effect '%s' does not exist"%sp[1])+Q)
            if (sp[1]=='state'):   return ('"%s"'%self.llcClass.state)
            elif (sp[1]=='iseffect'):
                if sp[2] in self.llcClass.effects.keys():
                    return('"true"')
                else:
                    return('"false"')
            else:
                # sp[1] is either a property name or an effect name. First check if it is a property
                # by trying to get it:
                try:
                    r = self.llcClass.GetProperty(sp[1])
                    # If we come here, it was a property and it was 'get' right, so return it
                    return r
                except:
                    pass
                
                # if we come here, it was not a property, so sp[1] should be an effect name
                try:
                    return self.llcClass.GetEffectProperty(sp[1],sp[2])
                except ParseError as L:
                    return Q+ERROR_PRE+L.__str__()+Q
                    
        elif (sp[0] == 'create'):
            # sp[1] is effect type, sp[2] is effect name
            try:
                self.llcClass.AddEffect(sp[2], sp[1])
            except ParseError as L:
                return (Q+ERROR_PRE+L.__str__()+Q)
            return '"Effect %s with name %s created"'%(sp[1], sp[2])
        elif(sp[0]=='remove'):
            if (sp[1] == 'all'):
                self.llcClass.RemoveAllEffects()
                return ('"Removed all effects"')
            else:
                try:
                    self.llcClass.RemoveEffect(sp[1])
                    return ('"Removed effect with name %s"'%sp[1])
                except ParseError as L:
                    return (Q+ERROR_PRE+L.__str__()+Q)
        elif (sp[0]=='calibrateforcesensors'):
            self.llcClass.CalibrateForceSensors()
            return '"Force sensors calibrated"'
        elif (sp[0]=='calibratepatient'):
            return '"Patient calibrated"'
        elif (sp[0]=='resettreadmilldistance'):
            self.llcClass.properties['treadmill'][3]=0.0
            return '"Treadmill distance reset to 0.0"'
        else:
            return (Q+(ERROR_PRE+"Unknown command '%s'" %sp[0])+Q)
        
    def Poll(self):
        """Does the real work: checks if something is waiting on this port, and
        if so, it returns data to the send port."""
        
        while True: # We return from this function if there is no more data
            # Read, if possible
            try:
                data,addr = self.recvSock.recvfrom( 1600 )
            except socket.error as T:
                if (T.errno == 10035):
                    # No incoming data
                    if (self.debugLevel>=3):
                        my_print ("No incoming data on port %d."%self.recvPort)
                    return
                elif (T.errno == 11 or T.errno == 35):
                    # "Resource temporarily unavailable"; I think this simply means there's no data.
                    # Probably error numbers in Windows and linux are different?
                    # I have: Mac: error 35 ('BlockingIOError')
                    #         Linux: 11?
                    if (self.debugLevel>=3):
                        my_print ('"Resource temporarily unavailable (port %d).'%self.recvPort)
                    return
                else:
                    raise # reraise error
            # If we're here, there is data.
            if (len(data)>1450 and self.debugLevel>=1):
                my_print ("Warning: length of incoming data on port %d is more than 1450 bytes!"% self.recvPort)
            
            # Split data, process each part separately and concatenate a new string
            data = data.decode()
            
            dataMutex.lock()
            self.llcClass.TouchMostRecentUDPMessageTime()
            answers = [self.ParseCommand(x) for x in data.split(";")]
            dataMutex.unlock()
            
            reply = ";".join(answers)
            
            replyBytes = reply.encode()
            if (len(replyBytes)>1450 and self.debugLevel>=1):
                my_print ("Warning: length of outgoing data on port %d is more than 1450 bytes!"%self.sendPort)
                    
            self.sendSock.sendto(reply.encode(),(addr[0],self.sendPort))
            self.nMessagesSent += 1
            if (self.debugLevel>=2):
                self.debugPrintSignal.emit("%5.2f Received on port %5d (length: %4d): *%s*"%(time.time()%100, self.recvPort, len(data), data))
                self.debugPrintSignal.emit("%5.2f Sent     to port %5d (length: %4d): *%s*"%(time.time()%100, self.sendPort, len(reply), reply))
                self.debugPrintSignal.emit("")
            # Done.
        
    def run(self):
        """Threading function"""
        self._stopbool = False
        self.isOn = True
        while (not self._stopbool):
            self.Poll()
            QtCore.QThread.msleep(self.delay*1000)
           
    def stop(self):
        """Makes the running thread stop."""
        self._stopbool = True
        self.isOn = False
        
    def SetCommunication(self,onOffToggle):
        """valid values for onOffToggle: 'on', 'off', 'toggle'"""
            
        if (onOffToggle=='toggle'): onOffToggle = 'on' if self.isOn==False else 'off'
        
        # If we're here, onOffToggle should be 'on' or 'off'
        if (onOffToggle=='off' and self.isOn):
            self.stop()
        elif (onOffToggle=='on' and not self.isOn):
            self.start()
        else:
            raise ValueError('Unknown value for onOffToggle: %s'%onOffToggle)

    def SetDebugLevel(self,n):
        self.debugLevel = n

    def GetDebugLevel(self):
        return self.debugLevel
        
    def __del__(self):
        """Destructor"""
        self.recvSock.close()
        self.sendSock.close()
        
class LLC(QtCore.QObject):
    myPrintSignal = QtCore.Signal(str) 
    
    def __init__(self, hlcIP = None, includeAnkles = True):
        """Initializes the LLC emulator.
        """
        QtCore.QObject.__init__(self)
        
        self._integrationMethod='rk4'
        self._includeAnkles = includeAnkles
        self._previousDynamicsTime = None # Force initialization
        self._integrationStepTime=0.01 # Step time in dynamics integration [s]
        self._mostRecentUDPMessageTime = 0
        self._watchdogEnabled = True
        defaultDebugLevel = 1
        self.debugLevel = defaultDebugLevel
        self.hlcIP = hlcIP
        self._x = []
        self._x.append(CommunicationChannel(25000,25001,self, debugLevel=defaultDebugLevel))
        self._x.append(CommunicationChannel(25002,25003,self, debugLevel=defaultDebugLevel))
        self._x.append(CommunicationChannel(25004,25005,self, debugLevel=defaultDebugLevel))
        self._x.append(CommunicationChannel(25006,25007,self, debugLevel=defaultDebugLevel))
        self._err = ErrorChannel(hlcIP, 30001, self, debugLevel = defaultDebugLevel)
          
        self.patientInLopes=False
        self.communicate = False
        self.state = 'off'
        self.effects = {}
        self.properties = {
            'inertia': PerReader().GetSegmentInertias(),
            # Not implemented yet in emulator 'coulombfriction'
            'workspacepatient':self.rangeOne(30),
            'treadmill':[0,0,0,0,0,0,0], # Special case; set treadmill is only scalar
            'patientdim':[80,1.8,0.35,0.2,0.5,0.5,0.5,0.5,0.3,0.3,0.06,0.06],
            'accelerationref': [0,0,0,0,0,0,0,0,0,0],
            'accelerationrefgain': 1.0, 
            'watchdogtimeout': 0 }
        self.measurements = { # and other things that are only readable (you may be able to reset them though)
            'segmentmeaspos': None, # These will all be set in the Dynamics() function
            'segmentmeasvel': None, 
            'segmentmeasacc': None, 
            'segmentmodelpos':None,
            'segmentmodelvel':None,
            'segmentmodelacc':None,
            'segmentmeasforce':10*[0], # Not set in the dynamics; it just returns 0's. (but at least it does return something)
            'jointmodelpos': None,
            'cartesianmodelpos': None, 
            'cartesianmeaspos':  None, 
           
            'isinitok': False,
            'ismotorsok': False,
            'isforcecalibrated': False,
            'isforcewithinbounds': False,
            'ispositioncalibrated': False,
            'ispositionwithinbounds': False,
            'ispositionconsistent': False,
            'iscabinetbluebuttonpressed': True,
            'ispatientdefined': True,# Temporary! False,
            'ispatientcalibrated': False,
            'ispatientconsistent': False,
            'istreadmillconsistent': True,
            'istreadmillempty': True,
            'isreadyforoperational':True
             }
        self._patientInertia = PatientInertia(self.properties['patientdim'][0],self.properties['patientdim'][1])
             
        # Parameters for rendering the end stops
        self._jointRanges = PerReader().GetJointRanges()
        self._endStopStiffnesses = [1e5, 1e5, 1e4, 1e4, 1e4, 1e4, 1e4, 1e4, 1e4, 1e4];
        self._endStopRelDamping = 10*[1]
        self._endStopMaxForce = [1000,1000]+ 8*[100]
        self._endStopDamping = CalculateDamping(self._endStopStiffnesses, self._endStopRelDamping, self.properties["inertia"])
        
        # Parameters for the 'external force spring', with which you can exert
        # forces on the force sensors (the forces are not directly applied to
        # the dynamics. I.e., in state position, this spring does nothing; in
        # state force it does do something. Exception on this: in state off,
        # the external forces are also applied. This more or less mimics the
        # behavior of the real LLC, which can also be 'freely' moved around in
        # state off.
        # Note that this spring behaves like a jointspring (not a segment spring)
        # 
        # Note: the Position, stiffenss and damping terms are an array of arrays:
        # The 'outer' level of arrays allows for different (simultaneous) sets of
        # external forces. Currently, we have two sets:
        # - self._externalForceSpringX[0][0:9] = ForceInteraction (add force/stiffness with mouse)
        # - self._externalForceSpringX[1][0:9] = ReplayTrajectory (acts as if the human tries to move according to a time-series from a file).
        self._externalForceSpringPosition = 2*[10*[0]]
        self._externalForceSpringStiffness = 2*[10*[0]]
        self._externalForceSpringDamping = 2*[CalculateDamping(self._externalForceSpringStiffness[0], 10*[0.7], self.properties["inertia"])]
        self._externalForceSpringMaxForce = 2*[[1000,1000]+ 8*[100]]
        
        # Damping (of motors) when system is off)
        self._offDampingTau = 0.1 # [s]
        self._offDamping = prodsc(self.properties["inertia"],-1/self._offDampingTau)
        
        # Calculate initial position of the system.    
        self._updateDynamics = True # Set to false to skip dynamics integration
        self.Dynamics()
        
        # Noise
        self._forceSensorNoiseMagnitude = 0.01
        
        self.stateTransitionTime = 0 # Indicates at what time we went to this state
        self.stopBool = False
        self.time = 0
        self.systemTimeAtTIsZero=time.time()
        self.fps = 15
        self.noiseAmount = 0.1
        
        self.HWM = UseHumanWalkModel(showFeet=True) # Must be within run() so that it is created wihtin this thread.
        self._llc_Graphics()
        self.SetCommunication('on');
        
        # start event loop in thread
        class GraphicsClass(QtCore.QObject):
            def __init__(self,parent):
                QtCore.QObject.__init__(self)
                self._parent = parent
                self._fps = self._parent.fps
                
                self._T_Graphics = QtCore.QTimer()
                self._T_Graphics.timeout.connect(self.Poll)
                self._T_Graphics.start(1000/self._parent.fps)
                
            def Poll(self):
                self._parent._llc_Graphics()
            
            def SetFps(self,value):
                self._fps = value
                print("Trying to alter fps")
                self._T_Graphics.setInterval(1000/self._fps)
                print("Done to alter fps")
               
        self._GraphicsThread = QtCore.QThread()
        self._GraphicsClass = GraphicsClass(self)
        self._GraphicsClass.moveToThread(self._GraphicsThread)
        self._GraphicsThread.start(QtCore.QThread.Priority.LowPriority) # Execute the run function in a separate thread
        
        # Set up timers (must be done in this thread)
        self._T_Behaviour = QtCore.QTimer()
        self._T_Behaviour.timeout.connect(self._llc_Behaviour)
        self._T_Behaviour.start(self._integrationStepTime*1000)
        

    def Dynamics(self):
        """This function simulates the dynamics of the system. It should be called regularly (i.e., within the run() loop).
        It keeps track of its local time itself, so if there's jitter in calling, this is not a big problem."""
        # Note: segmentmodelpos is 'driving': we calculate everything in that coordinate system and then convert to the other
        # coordinate systems.
        currentTime = time.time()
        if self._previousDynamicsTime is None:
            # Initialize system in a random position
            
            S = self.measurements['segmentmodelpos'] = [random.normalvariate(0,0.1) for x in range(15)]
            V = self.measurements['segmentmodelvel'] = 15*[0]
            A = self.measurements['segmentmodelacc'] = 15*[0]
            # Now the new segment angles are known, and we can update the other measurement variables
            self.measurements['segmentmeaspos'] = [random.normalvariate(x,0.00000001) for x in S]
            self.measurements['segmentmeasvel'] = [random.normalvariate(x,0.00000001) for x in V]
            self.measurements['segmentmeasacc'] = [random.normalvariate(x,0.00000001) for x in A]
            self.measurements['jointmodelpos'] = [S[4]-S[3], S[5]-S[4], S[8]-S[7], S[9]-S[8]]
            self.measurements['jointmodelvel'] = [V[4]-V[3], V[5]-V[4], V[8]-V[7], V[9]-V[8]]
            self.measurements['jointmodelacc'] = [A[4]-A[3], A[5]-A[4], A[8]-A[7], A[9]-A[8]]
            self.measurements['cartesianmeaspos']  = self.CalculateCartesianSegments(self.measurements['segmentmeaspos'],self.properties['patientdim'])
            self.measurements['cartesianmodelpos'] = self.CalculateCartesianSegments(self.measurements['segmentmodelpos'],self.properties['patientdim'])
            # Now we can update the vertical position of the whole system.
            self.measurements['segmentmeaspos'][10] = self.measurements['segmentmodelpos'][10]=self.measurements['cartesianmeaspos'][1]
        
        else:
            # Do the dynamics
            # Mass 'matrix', we assume it is diagonal. These are very approximate.
            if self.patientInLopes:
                M = sumel(self.properties['inertia'], self._patientInertia)
            else:
                M = self.properties['inertia']
                
            def f(t,v,x):
                """
                Function to make integration via RK4 easier. This function does:
                    
                state_dot = f (t,state), or in other words:
                          .       /          \ 
                [ a ] = [ v ] = f|  t, [ v ]  |
                [ v ]   [ x ]     \    [ x ] / 
                
                and this function can then be used by any integration method.
                """    
                # Gather all forces. We only calculate the forces for the 10 actuated dofs; 
                # in the end we append 5*[0] for the resulting acceleration to accomodate for non-actuated dofs.
                F_tot = 10*[0]
                if self.state in ['force','position']:
                    # Apply forces from effects
                    allEffects = self.effects.copy() # Local copy in order to avoid changes to the variable during for loop
                    for e in allEffects.values():
                        if not e['enabled']:
                            continue
                        if e['_type'] == 'segmentspring' and e['enabled']:
                            # Note: deadband not implemented!
                            positionError = diffel(e['pos'], x[0:10])
                            positionForce = prodel(positionError, e['stiffness'])
                            
                            velocityError = diffel(e['vel'], v[0:10])
                            velocityForce = prodel(velocityError, e['_damping'])
                            
                            force = limitel(sumel(positionForce, velocityForce), neg(e['maxforce']), e['maxforce'])
                            F_tot = sumel(F_tot,force)
                        elif e['_type'] == 'jointspring' and e['enabled']:
                            xd=e['pos']; vd = e['vel'] # desired pos/vel in joint space
                            x_joint = SegmentPositionsToJointPositions(x) # Current position
                            v_joint = SegmentPositionsToJointPositions(v)
                            positionError = diffel(xd, x_joint)
                            positionForce = prodel(positionError, e['stiffness'])
                            
                            velocityError = diffel(vd, v_joint)
                            velocityForce = prodel(velocityError, e['_damping'])
                            f = limitel(sumel(positionForce, velocityForce), neg(e['maxforce']), e['maxforce']) # total joint force in joint space
                            force_in_segmentspace = JointForcesToSegmentForces(f)
                            F_tot = sumel(F_tot,force_in_segmentspace)
                        else:
                            raise NotImplementedError("Dynamics of effect type %s not implemented"%e['_type'])
                

                # Joint endstops (implemented as spring-damper, as opposed to the real lopes where they are more sophisticated)
                # These are in joint space!
                if True: # Just to allow indentation
                    x_joint = SegmentPositionsToJointPositions(x) # Current position
                    v_joint = SegmentPositionsToJointPositions(v)
                    
                    positionError = 10*[0]
                    velocityError = 10*[0] # For non-limited joints, we keep the velocityError zero so that its effect is void
                    jointsToCheck = range(10) if self._includeAnkles else [0,1,2,3,4,6,7,8]
                    for i in jointsToCheck:
                        if x_joint[i]< self._jointRanges[i][0]:
                            positionError[i] = self._jointRanges[i][0] - x_joint[i]
                            velocityError[i] = 0 - v_joint[i] # Endstop doesn't move
                        elif x_joint[i]> self._jointRanges[i][1]:
                            positionError[i] = self._jointRanges[i][1] - x_joint[i]
                            velocityError[i] = 0 - v_joint[i] # Endstop doesn't move
                        else:
                            pass
                            
                    positionForce = prodel(positionError, self._endStopStiffnesses)
                    velocityForce = prodel(velocityError, self._endStopDamping)
                    
                    f = limitel(sumel(positionForce, velocityForce), neg(self._endStopMaxForce), self._endStopMaxForce) # total joint force in joint space
                    
                    force_in_segmentspace = JointForcesToSegmentForces(f)
                    F_tot = sumel(F_tot,force_in_segmentspace)
                     
                # External forces
                # Include sensor noise
                if self.state in ['force','off']:
                    externalForces = self._UpdateExternalForces(x=x, v=v, includeNoise=False, applyToSegmentMeasForce=False)
                    F_tot = sumel(F_tot, externalForces)
            
                # Friction when in state off
                if self.state=='off':
                    F_tot = sumel(F_tot, prodel(v[0:10], self._offDamping))
                
                # Acceleration feed forward (only in states force and position)
                if self.state in ['force','position']:
                    accelerationfeedforward = prodsc( self.properties['accelerationref'], self.properties['accelerationrefgain'])  + 5*[0]
                else:
                    accelerationfeedforward = 15*[0]
                    
                acceleration = sumel( divel(F_tot, M) + 5*[0] , accelerationfeedforward)
                velocity = v
                
                # Override when in init or home
                if self.state == 'home' or (self.state == 'init' and not self.measurements['ismotorsok']):
                    if self._includeAnkles:
                        homePosition = [0,0,0,0.1,-0.2,0,0,0.1,-0.2,0]
                        acceleration0,self.done = accForParabolicTrajectory(x[0:10],v[0:10],homePosition,10*[0.1],10*[0.3],tolerance=0.001,vtolerance=0.0001)
                    else:
                        # Omit the ankles
                        homePosition = [0,0,0,0.1,-0.2,0,0.1,-0.2]
                        a,self.done = accForParabolicTrajectory(x[0:5]+x[6:9],v[0:5]+v[6:9],homePosition,8*[0.1],8*[0.3],tolerance=0.001,vtolerance=0.0001)
                        acceleration0 = a[0:5] + [0] + a[5:8] + [0]
                    acceleration = acceleration0+5*[0]
                    velocity = v
                    if self.done:
                         self.measurements['ismotorsok'] = True
                return acceleration,velocity
            
            def forwardEuler(f, t,v,x,ts):
                """Apply forward euler using function f (which supplies the derivative of the state, given t and state being v,x) for a time step size ts"""
                a,dummy = f(t,v,x)
                v_new = sumel(v,prodsc(a,ts))
                x_new = sumel(x,prodsc(v,ts))
                return v_new,x_new,a
            
            def forwardEulerMultistep(f,t,v,x,ts,maxIntegrationStepTime=100):
                """Do a forward euler, but divide the time ts in N steps."""
                # We need to integrate a total of dt seconds, with keeping in mind the _maxIntegrationSteptTime
                N = math.ceil(ts / maxIntegrationStepTime) # Number of steps
                stepTime = ts / N
                for i in range(N):
                    v,x,a = forwardEuler(f,t+i*stepTime,v,x,stepTime)
                return v,x,a
                
            def RK4(f,t,v,x,ts):
                """Do a nice RK4-integration (see wikipedia for the implementation)"""
                hts = ts/2 # half_ts
                ka1,kv1 = f(t     , v           , x          )
                ka2,kv2 = f(t+hts , sumel(v , prodsc(ka1,hts)) , sumel(x , prodsc(kv1,hts)))
                ka3,kv3 = f(t+hts , sumel(v , prodsc(ka2,hts)) , sumel(x , prodsc(kv2,hts)))
                ka4,kv4 = f(t+ts  , sumel(v , prodsc(ka3,ts )) , sumel(x , prodsc(kv3,ts )))
                
                v_new = [ v_ + ts/6 * (ka1_ + 2*ka2_ + 2*ka3_ + ka4_) for v_,ka1_,ka2_,ka3_,ka4_ in zip(v,ka1,ka2,ka3,ka4)]
                x_new = [ x_ + ts/6 * (kv1_ + 2*kv2_ + 2*kv3_ + kv4_) for x_,kv1_,kv2_,kv3_,kv4_ in zip(x,kv1,kv2,kv3,kv4)]

                a_new = f (t+ts, v_new, x_new)[0]
                return v_new, x_new, a_new
                
            def RK4Multistep(f,t,v,x,ts,maxIntegrationStepTime=100):
                """Do a RK4 integration of ts seconds, but divide the time ts in so many 
                steps that the step time for each integration is at most maxIntegrationStepTime."""
                
                # We need to integrate a total of dt seconds, with keeping in mind the _maxIntegrationSteptTime
                N = math.ceil(ts / maxIntegrationStepTime) # Number of steps
                stepTime = ts / N
                try:
                    if self._printRK4Information:
                        print(N,stepTime)
                except:
                    self._printRK4Information = False
                for i in range(N):
                    v,x,a = RK4(f,t+i*stepTime,v,x,stepTime)
                return v,x,a

                
            # Do the integration. We allow a 5% larger sample time than the desired. If we go beyond that, the period is split up in parts
            # not larger than 105% of the desired sample time.
            dt = currentTime - self._previousDynamicsTime
            if self._integrationMethod=='rk4':
                self.measurements['segmentmodelvel'],self.measurements['segmentmodelpos'],self.measurements['segmentmodelacc'] = RK4Multistep(f, 0, self.measurements['segmentmodelvel'],self.measurements['segmentmodelpos'], dt,self._integrationStepTime*1.05)
            elif self._integrationMethod=='euler':
                self.measurements['segmentmodelvel'],self.measurements['segmentmodelpos'],self.measurements['segmentmodelacc'] = forwardEulerMultistep(f, 0, self.measurements['segmentmodelvel'],self.measurements['segmentmodelpos'], dt,self._integrationStepTime*1.05)
            else:
                raise ValueError("self._integrationMethod is not 'rk4' or 'euler'")
                
            if not self._includeAnkles:
                # Then we simply force the position, velocity and acc of the ankles to be zero (i.e., the 
                # pva of the foot equal to the lower leg).
                def KillAnkleDynamics(p): p[5]=p[4];p[9]=p[8]; # Applies to the list p (in-place assignment)
                KillAnkleDynamics(self.measurements['segmentmodelpos'])
                KillAnkleDynamics(self.measurements['segmentmodelvel'])
                KillAnkleDynamics(self.measurements['segmentmodelacc'])
                
            # Now the new segment angles are known, and we can update the other measurement variables
            S = self.measurements['segmentmodelpos']
            V = self.measurements['segmentmodelvel']
            A = self.measurements['segmentmodelacc']
            self.measurements['segmentmeaspos'] = [random.normalvariate(x,0.001) for x in S]# + [random.normalvariate(0,0.001) for x in range(5)]
            self.measurements['segmentmeasvel'] = [random.normalvariate(x,0.001) for x in V]# + [random.normalvariate(0,0.001) for x in range(5)]
            self.measurements['segmentmeasacc'] = [random.normalvariate(x,0.001) for x in A]# + [random.normalvariate(0,0.001) for x in range(5)]
            self.measurements['jointmodelpos'] = [S[4]-S[3], S[5]-S[4], S[8]-S[7], S[9]-S[8]]
            self.measurements['jointmodelvel'] = [V[4]-V[3], V[5]-V[4], V[8]-V[7], V[9]-V[8]]
            self.measurements['jointmodelacc'] = [A[4]-A[3], A[5]-A[4], A[8]-A[7], A[9]-A[8]]
            self.measurements['cartesianmeaspos']  = self.CalculateCartesianSegments(self.measurements['segmentmeaspos'],self.properties['patientdim'])
            self.measurements['cartesianmodelpos'] = self.CalculateCartesianSegments(self.measurements['segmentmodelpos'],self.properties['patientdim'])
            # Now we can update the vertical position of the whole system.
            self.measurements['segmentmeaspos'][10] = self.measurements['cartesianmeaspos'][1]
    
            # Update treadmill data. This is always linear, so Forward Euler is always good enough.
            t = self.properties['treadmill']
            if self.state is not 'off':
                t[1]=random.normalvariate(t[0],0.001) # measured (and actual) speed
            else:
                t[1] = 0
            t[2] = 0 # Acceleration not implemented'
            t[3]+=t[1]*dt # total distance
            #t[4:6] COP and FY not implemented
        self._previousDynamicsTime = currentTime
        
    def _llc_Behaviour(self):
        """This function is called periodically by a timer. It calculates the dynamics
        and state progressions etc. (i.e., as long as this function is called regularly,
        it will emulate the behaviour of the llc)."""
        dataMutex.lock()
        
        self.time += time.time()-self.systemTimeAtTIsZero
        
        # Watchdogtimer
        if self._watchdogEnabled and self.properties['watchdogtimeout']>0:
            dt = (time.time()-self._mostRecentUDPMessageTime)*1000 # in ms
            if self.state!='off' and dt > self.properties['watchdogtimeout']:
                self.RequestStateChange('off')
                if (self.debugLevel >=1):
                    self.myPrintSignal.emit ("LLC internal: State changed to 'off' because watchdog timed out")
        
        # Check state transitions
        if (self.state != 'off' and self.measurements['iscabinetbluebuttonpressed'] == False):
            self.RequestStateChange('off')
            if (self.debugLevel >=1):
                self.myPrintSignal.emit ("LLC internal: State changed to 'off' because emergency button was pressed")
            
        if (self.state == 'init'):
            # Should go to off after 2 seconds && motors ready
            if (self.time - self.stateTransitionTime >= 2.0 and self.measurements['ismotorsok']):
                self.measurements['isinitok'] = True
                self.RequestStateChange('off')

        # Update the dynamics
        if self._updateDynamics:
            self.Dynamics()
        self._UpdateExternalForces()
        
        dataMutex.unlock()
        
    def _llc_Graphics(self):
        """This function is called periodically by a timer. It updates the graphics animation."""
        dataMutex.lock()
        S = self.measurements['segmentmodelpos'][:]
        M = self.measurements['segmentmeaspos'][:]
        J = self.measurements['jointmodelpos'][:]
        dataMutex.unlock()
        
        #              PX    PZ    LHA   LHF   LKF    LAP    RHA    RHF   RKF    RAP    PY    PRY   PRX   LFRY  RFRY
        jointAngles = [S[0], S[1], S[2], S[3], -J[0], -J[1], -S[6], S[7], -J[2], -J[3], S[10],S[11],S[12],S[13],S[14]] 
        treadmillDistance = self.properties['treadmill'][3]
        self.HWM.Update(jointAngles,treadmillDistance)
        
        
    def Stop(self):
        """Stops the background thread"""
        self._GraphicsThread.quit()
        self._llcThread.quit()
        print("Stopped graphics and dynamics threads.")
            
    def Reset(self):
        """Go to 'default' state, this is the 'resetall' command which can be issued from the HLC"""
        self.state = 'off'
        self.properties['treadmill'][0] = 0
        
    def HardReset(self):
        """As if the system was restarted"""
        self.myPrintSignal.emit ("Warning: Hard reset probably doesn't work well (doesn't reset everything)!")
        self.state = 'off'
        self.properties['isinitok'] = False
        self.properties['iscabinetbluebuttonpressed'] = True
        self.time = 0
        self.systemTimeAtTIsZero = time.time()

    def CalibrateForceSensors(self, setTo = True):
        """Acts as if the force sensors were calibrated at this moment. You can also
        use this function to force the value of isForceCalibrated to something."""
        self.myPrintSignal.emit("Force sensors calibrated (isforcecalibrated set to %s)"%setTo)
        self.measurements['isforcecalibrated'] = setTo
            
    def RequestStateChange(self,reqState, force=False):
        """Returns tuple (b, s), with b (boolean) indicating if the transition was successful, s (string) the answer to the command.
        if force==True, isInitOk is altered if needed, in order to get the right circumstances to allow the transition."""
        def setState(reqState):
            self.state = reqState
            self.stateTransitionTime = self.time
            if (self.debugLevel>=1):
                self.myPrintSignal.emit ("LLC internal: State changed to '%s'"%self.state)
            return ('"state set to \'%s\'"'%reqState)
        
        # Check for valid values    
        if (reqState not in ['off','init','home','stop','position','force']):
            raise ParseError('Unknown state name')
            
        if  reqState == 'off':
            self._DisableAllEffects()
            return setState(reqState)
        elif reqState == 'init':
            if self.state != 'off':
                raise ParseError('"Could not transition to state init because we\'re not in state off."')
            elif (not self.measurements['ismotorsok']) and (not self.measurements['istreadmillempty']):
                raise ParseError('"Could not transition to init because treadmill is not empty and motors should be tested."')
            else:
                self._DisableAllEffects()
                return setState(reqState)
        elif reqState in ['home','stop','position','force']:
            if force and not self.measurements['isinitok']:
                self.measurements['isinitok']   = True
                self.measurements['ismotorsok'] = True
                if (self.debugLevel>=1):
                    self.myPrintSignal.emit("LLC internal: Forced isInitOk (and possibly isMotorsOk) to be True in order to allow state transition.")
            if self.measurements['isinitok']:
                if not self.measurements['isreadyforoperational']:
                    self._err.Send('Not ready for operational (error channel message)')
                    raise ParseError('Not ready for operational (reply to request)')
                else:
                    if self.state == 'off':
                        # Then we had a transition from off to a powered state. Set treadmill velocity to 0
                        self.properties['treadmill'][0] = 0
                
                    if reqState in ['home','stop']:
                        self._DisableAllEffects()
                    return setState(reqState)
            else:
                raise ParseError('"Could not transition to state \'%s\' because robot was not initialized yet."'%reqState)
        else:
            raise ParseError('"Could not transition from state \'%s\' to \'%s\' (maybe simply not implemented yet...)"' %(self.state, reqState) )
            
    def SetCommunication(self,onOffToggle,channels=None):
        """valid values for onOffToggle: 'on', 'off', 'toggle'
        if channels (a list of references to the channels) is given, then the command only applies to those channels."""
        if channels is None:
            channels = self._x
        for x in channels:
            x.SetCommunication(onOffToggle)
            
    def SetFps(self,fps):
        self.fps = fps
        self._GraphicsClass.SetFps(fps)

    def SetWatchdogEnabled(self,v):
        """Note: you can use this to fully disable the watchdog (even if an application requests an operating
        watchdog by setting set watchdogtimeout != 0). So, the watchdog works when both watchdogtimeout!=0 AND
        self._watchdogEnabled is True."""
        if v=='toggle':
            self._watchdogEnabled = not self._watchdogEnabled
        else:
            self._watchdogEnabled = v
        return True if self._watchdogEnabled else False
        
    def TouchMostRecentUDPMessageTime(self):
        """This function should be called upon reception of any UDP message. It updates ('touches')
        the variable which remembers at what time the most recent UDP message was received. This
        data is then used for the watchdog timer."""
        self._mostRecentUDPMessageTime = time.time()
        
    def SetIntegrationStepTime(self,t):
        self._integrationStepTime = t
        self._T_Behaviour.setInterval(1000*t)

    def _DisableAllEffects(self):
        """Disables all effects (which is done automatically when going to some states)."""
        for e in self.effects.keys():
            self.effects[e]["enabled"]= False
            
    def RemoveEffect(self,effectName):
        """Tries to remove the given effect. Raises a ParseError if the effect
        does not exist."""
        effectName = str.lower(effectName)
        if (effectName not in list(self.effects.keys())):
            raise ParseError("Failed to remove effect with name %s. Effect does not exist"%effectName)
        del (self.effects[effectName])
    
    def RemoveAllEffects(self):
        for x in self.effects.keys():
            del(self.effects[x])
        
    def AddEffect(self, effectName, type):
        """Adds an effect to the effect list. If the effect already exists, it raises an error.
        Type should be one of: 
         segmentSpring, segmentDamper, segmentBiasForce, or segmentShaker,
         jointSpring, jointDamper, jointBiasForce, or jointShaker.
        Not all are implemented yet.
        """
        type=str.lower(type)
        effectName = str.lower(effectName)
        
        if (self.effects.get(effectName) is not None):
            # Already exists
            self.myPrintSignal.emit("Effect %s with name %s NOT created. Type is incorrect or name already exists."%(type,effectName))
            raise ParseError("Effect %s with name %s NOT created. Type is incorrect or name already exists."%(type,effectName))
           
            
        if (type=='segmentspring'):
            # a 10d spring
            d = 10
            eff ={
                '_type': 'segmentspring',
                'pos':          d*[0],
                'vel':          d*[0],
                'maxforce':     d*[1e6],
                'stiffness':    d*[0],
                'dampfactor':   d*[0.7],
                'halfdeadband': d*[0],
                'enabled': False
            }
            eff['_damping']= CalculateDamping(eff['stiffness'],eff['dampfactor'], self.properties['inertia'])
            self.effects[effectName] = eff
        elif (type=='jointspring'):
            # a 10d spring
            d = 10
            eff = {
                '_type': 'jointspring',
                'pos':          d*[0],
                'vel':          d*[0],
                'maxforce':     d*[1e6],
                'stiffness':    d*[0],
                'dampfactor':   d*[0.7],
                'damping': d*[0.7], # Huh, is this one needed??
                'halfdeadband': d*[0],
                'enabled': False
            }
            eff['_damping']= CalculateDamping(eff['stiffness'],eff['dampfactor'], self.properties['inertia'])
            self.effects[effectName] = eff
        elif (type == 'spring'):
            # This type of effect is obsolete. My implementation was not good anyway,
            # so I decided to delete it.
            raise NotImplementedError("spring not implemented yet")
        elif (type=='segmentdamper'):
            raise NotImplementedError("segmentDamper not implemented yet")
        elif (type=='segmentbiasforce'):
            raise NotImplementedError("segmentbiasforce not implemented yet")
        elif (type=='segmentshaker'):
            raise NotImplementedError("segmentshaker not implemented yet")
        elif (type=='jointdamper'):
            raise NotImplementedError("jointDamper not implemented yet")
        elif (type=='jointbiasforce'):
            raise NotImplementedError("jointBiasForce not implemented yet")
        elif (type=='jointshaker'):
            raise NotImplementedError("jointShaker not implemented yet")
        else:
            raise ParseError("No effect type %s"%type)
            
            
    def SetEffectProperty(self, effectName, effectParameter, value=None):
        """Tries to set the effect parameter to value. Fails if effectName or effectParameter
        does not exist, or if value has noet the same number of elements as the original parameter.
        In that case, it raises a ParseError. Returns None if succeeded."""
        humanNames = {
            'segmentspring': 'Segment Spring',
            'segmentdamper': 'Segment Damper',
            'jointspring': 'Joint Spring',
            'jointdamper': 'Joint Damper',
            }
        effectName = str.lower(effectName)
        if (type(effectParameter) is str): effectParameter = str.lower(effectParameter)
        
        E = self.effects.get(effectName)
        if (E is None):
            # Effect does not exist
            raise ParseError('Effect \'%s\' does not exist'%effectName)
        effectType = E['_type']

        if (effectParameter == 'enable'):
            E['enabled'] = True
            return ('Effect enabled')
        elif (effectParameter == 'disable'):
            E['enabled'] = False
            return ('Effect disabled')
                
        # If we're here, we have a normal set with parameter.
        P = E.get(effectParameter)
        if (P is None or effectParameter == 'enabled'):
            # Parameter does not exist for this effect, or we try to set the 'enabled' parameter, which is forbidden (use enable/disable instead)
            raise ParseError("\'%s\' property does not exist for effect %s"%(effectParameter, humanNames[E['_type']]))
                            
        if (lenn(P) != lenn(value)): # lenn: advanced len
            # Wrong number of values
            if (lenn(P)==1):
                # raise ParseError('%s NOT set. %iD vector not in correct format)'%(effectParameter, effectName, lenn(P),lenn(value)))
                raise ParseError('%s NOT set. %iD vector not in correct format)'%(effectName, lenn(P)))
            else:
                # raise ParseError('%s NOT set. %iD vector not in correct format)'%(effectParameter, effectName, lenn(P),lenn(value)))
                raise ParseError('%s NOT set. %iD vector not in correct format)'%(effectName, lenn(P)))
    
        # If we're here, it is okay and we can assign:
        E[effectParameter] = value
        
        if effectParameter in ['stiffness', 'dampfactor']:
            E['_damping']= CalculateDamping(E['stiffness'],E['dampfactor'], self.properties['inertia'])
            
        replies= {
            'vel':'velocity',
            'pos':'position',
            'stiffness':'stiffness',
            'maxforce':'max force',
            'halfdeadband':'half deadband'}
        GeneralProperties = ['pos','vel','maxforce']
        reply = replies.get(effectParameter)
        effectTypeStr = humanNames[E['_type']]
        if (reply is None): reply = effectParameter
        if effectParameter=='stiffness':
            return 'Spring10D\'s %s set'%reply
        else:
            return 'Effect\'s %s set'%reply
        
    
    def GetEffectProperty(self,effectName,propertyName):
        """Returns the property of the effect (if it exists)"""
        E = self.effects.get(effectName)
        if (E is None):
            # effect does not exist
            raise ParseError('Effect \'%s\' does not exist'%effectName)
 
        # Normal property
        V = E.get(propertyName)
        if (V is None):
            # property does not exist in error.
            raise ParseError('"effect %s has no property %s"'%(effectName, propertyName))
        return self.FormatValueForUDPResponse(V)

    def SetStatusBoolean(self, propertyName, value):
        """Status booleans that can be set are: isinitok, ispatientdefined and ispatientcalibrated.
        They can only be set to false, not to true!"""
        humanNames = {
                'isinitok':'Init OK',
                'ispatientdefined':'Patient Defined',
                'ispatientcalibrated':'Patient Calibrated'}
        if (propertyName in ['isinitok', 'ispatientdefined','ispatientcalibrated']):
            if (value == False):
                self.measurements[propertyName] = False
                return '"'+humanNames[propertyName]+' set to \'false\'"'
            if(value==True):
                raise ParseError('%s can be set only to \'false\''%humanNames[propertyName])
            else:
                raise ParseError('%s can be set only to \'false\''%humanNames[propertyName])
        else:
            raise ParseError('No settable status boolean %s.'%propertyName)
        
    def SetProperty(self, propertyName, value=None):
        """Tries to set the property (e.g., inertia, coulombfriction, ... Fails if propertyName
        does not exist, or if value has not the same number of elements as the original value.
        In that case, it raises a ParseError. Returns None if succeeded."""
        humanNames = {
            'inertia': 'Inertia',
            'coulombfriction': 'NOT IMPLEMENTED!',
            'workspacepatient': 'Workspace patient',
            'accelerationref': 'Acceleration reference',
            'accelerationrefgain': 'Acceleration reference gain',
            'treadmill': 'Treadmill speed',
            'patientdim': 'Patient dimensions',
            'watchdogtimeout': 'Watch dog timeout'}

        propertyName = str.lower(propertyName)
        
        P = self.properties.get(propertyName)
        if (P is None):
            # propertyName does not exist
            raise ParseError('property %s does not exist'%effectName)
            
        if (propertyName=='watchdogtimeout' and value<0):
            raise ParseError('Watch dog timeout NOT set. Value must be >= 0.0')
            
        # If we're here, we have an existing property.
        if propertyName == 'treadmill':
            # special case. The property is length 7, but we can only set the velocity as a  scalar
            if (self.IsSameType(1.0,value)): # Check if the argument is one number
                self.properties[propertyName][0] = value
            else:
                raise ParseError('%s NOT set. Parameter is not a number'%(humanNames[propertyName]))
                
        elif (not self.IsSameType(P,value)):
            # Wrong number of values
            if (lenn(P)==1):
                raise ParseError('%s NOT set. Parameter is not a number'%(humanNames[propertyName]))
            else:
                raise ParseError('%s NOT set. %iD vector not in correct format'%(humanNames[propertyName], lenn(P)))
        else:
            # If we're here, the assignement can be done successfully
            self.properties[propertyName] = value
            
            if propertyName=='inertia':
                # Then we need to recalculate the damping values for all springs and endstops
                self._endStopDamping = CalculateDamping(self._endStopStiffnesses, self._endStopRelDamping, value)
                for i in range(len(self._externalForceSpringDamping)):
                    self._externalForceSpringDamping[i] = CalculateDamping(self._externalForceSpringStiffness[i], 10*[0.7], value)
                self._offDamping = prodsc(self.properties["inertia"],-1/self._offDampingTau)
        
                for eff in self.effects.values():
                    if eff['_type'] in ['segmentspring','jointspring']:
                        eff['_damping']= CalculateDamping(eff['stiffness'],eff['dampfactor'], value)
                    else:
                        raise NotImplementedError("Need to implement setting inertia for effect of type %s."%eff['_type'])
            elif propertyName=='patientdim':
                self._patientInertia = PatientInertia(value[0], value[1])
                

        # Replies are custom per command, here is a mapping:
        return '"'+humanNames[propertyName]+' set"'
        
    def GetProperty(self,propertyName):
        """Checks the list of properties and measurements and returns the given property's value"""
        v = self.properties.get(propertyName)
        if (v is None):
            v = self.measurements.get(propertyName)
        
        if (v is None):
            raise ParseError('property %s does not exist'%propertyName)
            
        return self.FormatValueForUDPResponse(v)
            
    def FormatValueForUDPResponse(self,v):
        """If v is a list of floats, it formats it as [1.000000,2.00000,...]
        If v is a bool, it returns "true" or "false" (WITH quotes),
        otherwise it returns the string surrounded by quotes."""
        if (lenn(v) > 1): # A vector
            return "[" + ",".join([self.FormatValueForUDPResponse(x) for x in v])+"]"
        else:
            # A scalar
            if (type(v) is list): v=v[0]
            
            if (type(v) in (float, int, np.float64)): return '%f'%v
            elif (type(v) is bool): return '"true"' if v else '"false"'
            else: return '"'+v+'"'

    def IsSameType(self,a,b):
        """Returns true if a and b are, recursively, of the same type.
        Lists are considered same type if they have identical length and
        each element is of same type in both lists.
        Ints and floats are considered same type (both numeric)"""
        
        # If both are list
        if (type(a) is list and type(b) is list):
            if (len(a) != len(b)): return False
            else: return True if all([self.IsSameType(aa,bb) for (aa,bb) in zip(a,b)]) else False
            
        if (type(a) in (int,float) and type(b) in (int,float)): return True
        if (type(a) is type(b)): return True
        return False
        
    def rangeOne(self,n):
        """Returns a (real list) range with n elements starting from one"""
        return list(range(1,1+n))
        
    def setNoiseAmount(self, x):
        """"Use to set the amount of noise in some unit (1 is quite a lot of noise)"""
        self.noiseAmount = x
        
    def SetHLCIp(self,ip):
        self.hlcIP = ip;
        self._err.SetHLCIp(ip)
        
    def SetUpdateDynamics(self, value = None):
        if value is None:
            self._updateDynamics = not self._updateDynamics
        else:
            self._updateDynamics = value
            
    def SetExternalForceSpring(self, i, position, stiffness):
        """This function can be used to set the parameters of a spring which
        acts as an external force. Index i determines which instance is updated
        (see the comments at initialization of self._externalForceSpringX)."""
        self._externalForceSpringPosition[i] = position
        # Only update stiffness and damping if needed
        if self._externalForceSpringStiffness[i]!= stiffness:
            self._externalForceSpringStiffness[i] = stiffness
            self._externalForceSpringDamping[i] = CalculateDamping(self._externalForceSpringStiffness[i], 10*[0.7], self.properties["inertia"])
        
    def _UpdateExternalForces(self, x=None, v=None, includeNoise = True, applyToSegmentMeasForce = True):
        """This function serves two purpopses: 
        1. It can calcualte external forces and apply it to self.measurements['segmentmeasforce'], or
        2. It can calculate the external forces without noise, used for the integration (including intermediate results in minor steps)
        
        Given the parameters of the externalForceSpring and the current
        state of the robot, it updates the state of self.properties["segmentmeasforce"].
        This consists of a part applied the ExternalForcesSpring (mimicing of a real spring if it
        were attached on the robot. Note that the externalForceSpring behaves like
        a jointspring) and some noise."""
        force_in_segmentspace = 10*[0]
        for i in range(len(self._externalForceSpringPosition)):
            
            xd=self._externalForceSpringPosition[i]; vd = 10*[0]  # desired pos/vel in joint space
            if x is None:
                x = self.measurements['segmentmodelpos']
                v = self.measurements['segmentmodelvel']
            x_joint = SegmentPositionsToJointPositions(x) # Current position
            v_joint = SegmentPositionsToJointPositions(v)
            positionError = diffel(xd, x_joint)
            positionForce = prodel(positionError, self._externalForceSpringStiffness[i])
            velocityError = diffel(vd, v_joint)
            velocityForce = prodel(velocityError, self._externalForceSpringDamping[i])
            f = limitel(sumel(positionForce, velocityForce), neg(self._externalForceSpringMaxForce[i]), self._externalForceSpringMaxForce[i]) # total joint force in joint space
            
            # Add to total force_in_segmentspace
            force_in_segmentspace = sumel(force_in_segmentspace, JointForcesToSegmentForces(f))
        
        if includeNoise:
            force_in_segmentspace = sumel(force_in_segmentspace, [random.normalvariate(0,self._forceSensorNoiseMagnitude) for x in range(10)])
        
        if applyToSegmentMeasForce:
            self.measurements['segmentmeasforce'] = force_in_segmentspace
        else:
            return force_in_segmentspace
        
        

    def CalculateCartesianSegments(self,segmentangles,patientDim):
        """Returns a list of cartesian positions of some parts of the body (11x3D)
        
        function [pPelvis, pLeftHip, pLeftKnee, pLeftAnkle, pLeftHeel, pLeftToe, pRightHip, pRightKnee, pRightAnkle, pRightHeel pRightToe]=
                CalculateCartesianSegments(segmentangles,patientDim)
                
        segmentangles are as described in API 2.6.
        
        """
        
        def rotx(alpha):
            """Returns rotation matrix after rotation around x (which direction??)"""
            c = math.cos(alpha); s = math.sin(alpha)
            return np.matrix( [[1,0,0],[0,c,-s],[0,s,c]])
    
        def rotz(alpha):
            """Returns rotation matrix after rotation around z (which direction??)"""
            c = math.cos(alpha); s = math.sin(alpha)
            return np.matrix( [[c,-s,0],[s,c,0],[0,0,1]])
    
        def p(x,y,z):
            """Make a numpy.matrix from 3 coordinates"""
            return np.matrix([[x],[y],[z]])
        def l(p):
            """Make a list from a numpy.matrix"""
            return [np.asscalar(p[i]) for i in range(len(p))]
            
        # The function itself...
        
        pelvisWidth    = patientDim[2] 
        upperLegLength = patientDim[4]
        lowerLegLength = patientDim[6]
        footLength     = patientDim[8]
        ankleHeight    = patientDim[10]
    
        forward,sideways,ltRX, ltRZ, lsRZ, lfRZ, rtRX, rtRZ, rsRZ, rfRZ = segmentangles[0:10] # We omit the others
    
        # We don't know which foot is on the floor; neither the height of 
        # the hip. We do it simple: calculate the height of each leg (i.e., 
        # vertical distance between ankle/toe and hip); and assume that the
        # longest leg is on the floor.
    
        # NOTE THAT THESE CALCULATIONS ARE ROUGH APPROXIMATIONS!!
    
        
        p_LeftHip_Pelvis = p(0,0, -pelvisWidth/2)
        p_LeftKnee_LeftHip = rotx(ltRX) * rotz(ltRZ) * p(0,-upperLegLength,0)
        p_LeftAnkle_LeftKnee = rotz(lsRZ) * p(0,-lowerLegLength,0)
        p_LeftHeel_LeftAnkle = rotz(lfRZ) * p(-footLength*0.2, -ankleHeight,0)
        p_LeftToe_LeftAnkle = rotz(lfRZ) * p(footLength*0.8, -ankleHeight,0)
        
        p_RightHip_Pelvis = p(0,0, pelvisWidth/2)
        p_RightKnee_RightHip = rotx(rtRX) * rotz(rtRZ) * p(0,-upperLegLength,0)
        p_RightAnkle_RightKnee = rotz(rsRZ) * p(0,-lowerLegLength,0)
        p_RightHeel_RightAnkle = rotz(rfRZ) * p(-footLength*0.2, -ankleHeight,0)
        p_RightToe_RightAnkle  = rotz(rfRZ) * p( footLength*0.8, -ankleHeight,0)
        
        # Calculate the positions if the pelvis were at (0,0,0)
        p_Pelvis = p(0,0,0)
        p_LeftHip    = p_Pelvis    + p_LeftHip_Pelvis
        p_LeftKnee   = p_LeftHip   + p_LeftKnee_LeftHip
        p_LeftAnkle  = p_LeftKnee  + p_LeftAnkle_LeftKnee
        p_LeftHeel   = p_LeftAnkle + p_LeftHeel_LeftAnkle
        p_LeftToe    = p_LeftAnkle + p_LeftToe_LeftAnkle
        
        p_RightHip   = p_Pelvis     + p_RightHip_Pelvis
        p_RightKnee  = p_RightHip   + p_RightKnee_RightHip
        p_RightAnkle = p_RightKnee  + p_RightAnkle_RightKnee
        p_RightHeel  = p_RightAnkle + p_RightHeel_RightAnkle
        p_RightToe   = p_RightAnkle + p_RightToe_RightAnkle
        
        # Find the 'lowest' point (either an ankle or a toe) and add that to all elements:
        heightOffset = -np.asscalar(min( [p_LeftHeel[1], p_LeftToe[1], p_RightHeel[1], p_RightToe[1] ] ))
        p_Pelvis = p(forward,heightOffset,sideways)
        p_LeftHip    += p_Pelvis
        p_LeftKnee   += p_Pelvis
        p_LeftAnkle  += p_Pelvis
        p_LeftHeel   += p_Pelvis
        p_LeftToe    += p_Pelvis
        
        p_RightHip   += p_Pelvis
        p_RightKnee  += p_Pelvis
        p_RightAnkle += p_Pelvis
        p_RightHeel  += p_Pelvis
        p_RightToe   += p_Pelvis
        
        # Return one big list (+ is list concatenator)
        return l(p_Pelvis)+ \
                l(p_LeftHip) + l( p_LeftKnee) + l( p_LeftAnkle) + l( p_LeftHeel) + l( p_LeftToe) + \
                l(p_RightHip) + l( p_RightKnee) + l( p_RightAnkle) + l( p_RightHeel) + l(p_RightToe)
        
        
if __name__=='__main__':
    l=LLC()
    l._x[0].ParseCommand('create segmentspring z')
    l.effects['z']['stiffness']=10*[1000]
    print(l.effects)
    l._x[0].ParseCommand('set inertia [1,2,3,4,5,6,7,8,9,10]')
    print(l.effects)
    l._x[0].ParseCommand('set z stiffness [1,2,3,4,5,6,7,8,9,10]')
    print(l.effects)
    
    