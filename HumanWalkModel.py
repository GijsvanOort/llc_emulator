# -*- coding: utf-8 -*-

""" 

This module implements a model of a walking humanoid. Angles of hip,
knee, ankle and toes can be specified.

Axis definitions in anatomy:  
x-asis -> forward (walking direction)
y-axis -> up
z-axis -> right

But in robotics z is up and the y-axis is left/right.

All limbs are created with their proximal joint center in (0,0,0) and 
the distal joint somewhere at negative z (pointing down the z-axis). 
So essentially we're using the robotics coordinate system. The conversion
to anatomical coordinates is done at the latest possible stage of 
visualization (with a 3D camera we would set the axis.zLabel to 'y').
Sometimes positions are expressed in anatomic "units". In these cases
this is done as explicit as possible.

All units are in mm.

All limbs consist of a sphere joint followed by a semicone. The semicone
is open on the distal part, but this does not matter as the joint of the
next limb will fit there. One exception is the toe, which will have an
extra sphere to close off the model.
""" 

import visvis as vv
from visvis import Point, Pointset
import numpy as np
from math import sin, cos
import time, os
from pyzolib import ssdf, paths


COLOR_LEFT =    (0.16, 0.58, 0.80) # 40, 149, 204
COLOR_RIGHT =   (0.80, 0.16, 0.58) # 204, 40, 149
COLOR_REF =     (0.16, 0.80, 0.16) # 40, 204, 40
COLOR_OTHER =   (0.4,0.4,0.4)
WALK_RIGHT = True

# todo: anti-aliasing: looks good on black bg, but on white the edges are jaggy

# Define offsets
ANKLE_OFFSET = -0.47*np.pi
TOES_OFFSET = -0.01*np.pi


class HumanWalkModel(vv.Wobject):
    """ The base model that we use to illustrate a walking human.
    
    This model has two legs; lega and legb. legb is in front of lega.
    In case both legs should be shown, lega and legb are the left and
    right leg, respectively. In case one leg should be shown together
    with a reference, lega is the reference, and legb is the actual leg.
    
    This is the main interface to the model. Use the 'left' and 'right'
    properties to access both legs.
    """ 
    
    def __init__(self, parent=None, showFeet = True):
        vv.Wobject.__init__(self, parent)
        self._redrawTimes = []
        self._showFeet = showFeet
        # Create Pelvis
        self._pelvisWidth = 0.2 # m
        
        # Create legs
        self._lega = LegModel(self, None, True,  self._showFeet) 
        self._legb = LegModel(self, None, False, self._showFeet)
        
        # Set rotation
        self._rotation = vv.misc.Transform_Rotate(0.0, -1.0, 0.0, 0.0)
        self.transformations.append(self._rotation)
        self._CorrectOrientation()
        
        # Set initial attachment point, align at 0,0 by default
        self.anatomicReferenceX = 0.0
        self.anatomicReferenceY = 0.0
        self.anatomicReferenceZ = 0.0
        self.pelvisAxialRotation = 0.0
        self.pelvisObliquity = 0.0
        
        # create treadmill
        self._treadmill = Treadmill(self)
    
    
    def UseLeftAndRight(self, attachementJoint='hip'):
        """ Use the left and right legs. 
        Calling this function sets the colors of the legs accordingly.
        """
        self.lega.SetColor(COLOR_LEFT)
        self.legb.SetColor(COLOR_RIGHT)
    
    # Properties for leg A and its aliases
    @property
    def lega(self):
        """ Property: leg a"""
        return self._lega
    @property
    def leftLeg(self):
        """ Property: alias for leg a"""
        return self._lega
    @property
    def refLeg(self):
        """ Property: alias for leg a"""
        return self._lega
    
    # Properties for leg B and its aliases
    @property
    def legb(self):
        """Property: leg b"""
        return self._legb
    @property
    def rightLeg(self):
        """Property: alias for leg b"""
        return self._legb
    @property
    def realLeg(self):
        """Property: alias for leg b"""
        return self._legb
    
    
    @vv.misc.PropWithDraw
    def anatomicReferenceX():
        """Property: get or set the x-position of the anatomic reference"""
        def fget(self):
            return self._anatomicReferenceX
        def fset(self, value):
            self._anatomicReferenceX = float(value)
        return locals()
    
    @vv.misc.PropWithDraw
    def anatomicReferenceY():
        """Property: get or set the y-position of the anatomic reference"""
        def fget(self):
            return self._anatomicReferenceY
        def fset(self, value):
            self._anatomicReferenceY = float(value)
        return locals()
    
    @vv.misc.PropWithDraw
    def anatomicReferenceZ():
        """Property: get or set the z-position of the anatomic reference"""
        def fget(self):
            return self._anatomicReferenceZ
        def fset(self, value):
            self._anatomicReferenceZ = float(value)
        return locals()
    
    @vv.misc.PropWithDraw
    def pelvisAxialRotation():
        """Property: get or set the axial rotation of the pelvis; Right hip forward+left hip backward is positive."""
        def fget(self):
            return self._axialRotation
        def fset(self,value):
            self._axialRotation = float(value)
        return locals()
        
    @vv.misc.PropWithDraw
    def pelvisObliquity():
        """Property: get or set the pelvis obliquity; Left hip up+right hip down is positive."""
        def fget(self):
            return self._pelvisObliquity
        def fset(self,value):
            self._pelvisObliquity = float(value)
        return locals()
    
    
    def OnDraw(self):
        """Updates the scene when it is drawn by visvis"""
        # Set rotation to 0 so that the translation coordinates map
        # exactly right
        self._redrawTimes.append(time.time())
        self._rotation.angle = 0
        
        pPelvis = vv.Point(self._anatomicReferenceX, -self._anatomicReferenceZ, self._anatomicReferenceY)
        
        for leg, hipDirection in [ (self.lega, -1),
                                   (self.legb, 1)]:
            
            # Calculate where the hips are relatively to the pelvis midpoint. We don't use transformations
            # of visvis for this, because we still want the orientation of the legs aligned with the world axes.
            c1 = cos(self.pelvisObliquity); s1 = sin(self.pelvisObliquity)
            p1 = np.matrix( [[1,0,0],[0,c1,-s1],[0,s1,c1]])
            c2 = cos(self.pelvisAxialRotation); s2 = sin(self.pelvisAxialRotation);
            p2 = np.matrix( [[c2,0,s2],[0,1,0],[-s2,0,c2]])
            hipPosition = np.matrix( [[0],[0],[hipDirection * 1000*self._pelvisWidth/2]])
            p3 = np.matrix([[1,0,0],[0,0,-1],[0,1,0]]) # Correct for axis conventions
            hipOffset = p3* p2 * p1 * hipPosition
            pHip = pPelvis + vv.Point(hipOffset.tolist())
            
            leg._legTranslation.dx = pHip.x
            leg._legTranslation.dy = pHip.y
            leg._legTranslation.dz = pHip.z

        # Auto-correct orientation
        self._CorrectOrientation()
    
    
    def _CorrectOrientation(self):
        """ Set orientation so that both in 2D and 3D camera the legs
        look good.
        """
        a = self.GetAxes()
        if a and a.cameraType == '2d':
            self._rotation.angle = 90
        else:
            self._rotation.angle = 0
    
    def PrepareAxes(self):
        """ Prepare the axes in which the model is shown.
        Sets limits, lights etc.
        """ 
        a = self.GetAxes()
        
        # Set some properties for the axes
        if WALK_RIGHT:
            a.daspect = 1, 1, 1
        else:
            a.daspect = -1, 1, 1
        a.daspectAuto = False
        self._CorrectOrientation()
        a.SetLimits(None,None,[0,1300])
        
        # Set lights
        # Use large amount of ambient lights, otherwise small overlaps between
        # legs are hard to see.
        a.light0.specular = 0.15
        a.light0.ambient = 0.8
        a.light0.diffuse = 0.2
        a.light0.position = 0, 0.0, 1, 0
        #
#         L1 = a.lights[1]
#         L1.On()
#         L1.color = 'w'
#         L1.diffuse = 0.25
#         L1.specular = 0.0
#         L1.position = 30,30,20, 0
#         #
#         L2 = a.lights[2]
#         L2.On()
#         L2.color = 'w'
#         L2.diffuse = 0.25
#         L2.specular = 0.0
#         L2.position = -30,-30,20, 0
    
    def Demo(self):
        """ Show a certain motion pattern for the model.
        This method returns a function which should be called periodically
        to show a walking model.
        """ 
        
        # Get ready
        self.UseLeftAndRight('hip')
        a = self.GetAxes()
        
        self.lega.toeAngle = 0
        def iter(t=None):
            
            # Put in new position
            if t is None:
                t = time.time()
            self.lega.hipFlexionAngle = np.cos(t) * 0.5 
            self.lega.kneeAngle = np.cos(t-0.2) * 0.4 + 0.4 
            self.lega.ankleAngle = np.cos(t-0.2) * 0.2
            
            t += np.pi
            self.legb.hipFlexionAngle = np.cos(t) * 0.5
            self.legb.kneeAngle = np.cos(t-0.2) * 0.4 + 0.4
            self.legb.ankleAngle = np.cos(t-0.2) * 0.2
            
            # Draw
            a.Draw()
            vv.processEvents()
            time.sleep(0.01)
        iter()
        # vv.core.baseFigure.printFPS = True
        return iter
    

    @vv.misc.PropWithDraw
    def showTriangles():
        """ Property: whether to also show the vertices (triangles) of the model.
        """ 
        def fget(self):
            return self.lega._uleg.edgeShading == 'plain'
        def fset(self, value):
            # Set value
            if value:
                value = 'plain'
            else:
                value = None
            # Apply
            for leg in [self.lega, self.legb]:
                for name in ['_uleg', '_lleg', '_foot', '_toes']:
                    m = getattr(leg, name)
                    m.edgeShading = value
        return locals()
        
    def setFeetVisible(self,visible):
        self._lega.SetFootVisible(visible)
        self._legb.SetFootVisible(visible)

class Treadmill(vv.Wobject):
    """ Represents the treadmill."""
    DISTANCE = 1000 # mm % Distance between the boxes
    N=7
    HEIGHT=5 # mm
    WIDTH=1000 # mm
        
    def __init__(self,parent):
        self._boxes = []
        for i in range(self.N):
            b = vv.solidBox(scaling=(self.DISTANCE/2,self.WIDTH,self.HEIGHT),axesAdjust=False)
            b.offset = self.DISTANCE * (i-(self.N/2))
            b.visible=False
            #b.translation=[b.offset,0,-self.HEIGHT/2]
            self._boxes.append(b)
            
    def setDistance(self,d):
        """Displaces the boxes such that it seems as if the treadmill has traveled
        distance d in [m] in total"""
        dm = - ((d*1000) % self.DISTANCE)
        for b in self._boxes:
            b.visible=True
            b.translation = [b.offset + dm,0,-self.HEIGHT/2]
            

class LegModel(vv.Wobject):
    """ Represents a leg. 
    
    This object contains the meshes for the different
    limbs. It allows setting the angles between the limbs (at the joints). 
    """
    
    def __init__(self, parent, offset=0, isLeft=True, showFeet=True):
        vv.Wobject.__init__(self, parent)
        self._showFeet = showFeet
        # Create limbs for this leg
        if True:
            m_uleg, m_lleg, m_foot, m_toes = loadLimbs()
            self._uleg = Limb(self, m_uleg, isLeft = isLeft)
            self._lleg = Limb(self._uleg, m_lleg, isLeft = isLeft, close = self._showFeet)
            self._foot = Limb(self._lleg, m_foot, isLeft = isLeft)
            self._toes = Limb(self._foot, m_toes, isLeft = isLeft)
        else:
            self._uleg = Limb(self,       ULEG_MEASURE)
            self._lleg = Limb(self._uleg, LLEG_MEASURE, isLeft = isLeft, close = self._showFeet)
            self._foot = Limb(self._lleg, FOOT_MEASURE, isLeft = isLeft)
            self._toes = Limb(self._foot, TOES_MEASURE, isLeft = isLeft, close=True)
        
        # Set offset. 
        # self._legTranslation is also used by HumanWalkModel to align joints
        offset = offset or 0.0
        self._legTranslation = vv.misc.Transform_Translate(0, offset, 0)
        self.transformations.append(self._legTranslation)
        self.offset = offset
        self.isLeft = isLeft
        # Set color
        self.SetColor(COLOR_OTHER)
        
        # Simply hide feet if we don't want them
        if not self._showFeet:
            self._foot.visible=False # Also applies to children (i.e., toes)
    
    def SetColor(self, color):
        """ Set the color of all limbs.
        """
        for m in [self._uleg, self._lleg, self._foot, self._toes]:
            m.faceColor = color
    
    @vv.misc.PropWithDraw
    def hipFlexionAngle():
        # note the minus, because hip angle is defined as flexion
        def fget(self):
            return - self._uleg.angle
        def fset(self, value):
            self._uleg.angle = - value
        return locals()
        
    @vv.misc.PropWithDraw
    def hipAbductionAngle():
        # note the minus, because hip angle is defined as flexion
        def fget(self):
            return - self._uleg.abangle
        def fset(self, value):
            self._uleg.abangle = - value
        return locals()    
    
    @vv.misc.PropWithDraw
    def kneeAngle():
        def fget(self):
            return self._lleg.angle
        def fset(self, value):
            self._lleg.angle = value
        return locals()
    
    @vv.misc.PropWithDraw
    def ankleAngle():
        # Note the offset to make it anatomically correct
        def fget(self):
            return self._foot.angle - ANKLE_OFFSET
        def fset(self, value):
            self._foot.angle = value + ANKLE_OFFSET
        return locals()
        
    @vv.misc.PropWithDraw
    def ankleEndorotationAngle():
        # Note sign change for left foot is done within foot.abangle
        def fget(self):
            return self._foot.abangle
        def fset(self,value):
            self._foot.abangle = value
        return locals()
    
    @vv.misc.PropWithDraw
    def toeAngle():
        def fget(self):
            return self._toes.angle - TOES_OFFSET
        def fset(self, value):
            self._toes.angle = value + TOES_OFFSET
        return locals()
        
    def SetFootVisible(self,visible):
        self._foot.visible=visible


class Limb(vv.Mesh):
    """ This is an overloaded Mesh class to give it a "angle" property,
    and to handle mesh instantiation during initialization.
    """
    def __init__(self, parent, meshOrMeasure, close=False, isLeft=True):
        
        # Is a basemesh given, or a measure, in wich case the limbs are
        # calculated on the fly ...
        self.isLeft = isLeft
        
        if isinstance(meshOrMeasure, vv.BaseMesh):
            baseMesh = meshOrMeasure
            self._limbLength = baseMesh._limbLength
        elif isinstance(meshOrMeasure, LimbMeasure):
            limbMeasure = meshOrMeasure
            
            # Get previous measure
            prevMeasure = None
            if isinstance(parent, self.__class__):
                prevMeasure = parent._limbMeasure
            
            # Get mes
            baseMesh = createLimb(limbMeasure, prevMeasure, close)
            
            # Store our measures
            self._limbMeasure = limbMeasure
            self._limbLength = limbMeasure.length
        else:
            raise ValueError('Invalid value for limb measure')
        
        # Create ourselves    
        vv.Mesh.__init__(self, parent, baseMesh)
        
        # Set translation according to parent length
        if isinstance(parent, self.__class__):
            tt = vv.misc.Transform_Translate(0, 0, -parent._limbLength)
            self.transformations.append(tt)
        
        # Allow rotation
        inRadians = False
        tr = vv.misc.Transform_Rotate(0.0, 0,1,0, inRadians)
        self.transformations.append(tr)
        self._rotateTransform = tr
        
        # Allow abduction rotation
        inRadians = False
        abtr = vv.misc.Transform_Rotate(0.0, 1,0,0, inRadians)
        self.transformations.append(abtr)
        self._abrotateTransform = abtr
        
        # Set lighting properties
        self.diffuse = 1.0
        self.ambient = 1.0
        self.specular = 1.0
        self.shininess = 25
    
    @vv.misc.PropWithDraw
    def angle():
        """ Get/Set the rotation of this limb (in radians).
        """
        def fget(self):
            return self._rotateTransform.angle * 3.141592/180
        def fset(self, value):
            self._rotateTransform.angle = float(value) * 180/3.141592
        return locals()
        
    @vv.misc.PropWithDraw
    def abangle():
        """ Get/Set the abduction rotation of this limb (in radians).
        """
        def fget(self):
            if self.isLeft:
                return -self._abrotateTransform.angle * 3.141592/180
            else:
                return self._abrotateTransform.angle * 3.141592/180
            
        def fset(self, value):
            if self.isLeft:
                self._abrotateTransform.angle = -float(value) * 180/3.141592
            else:
                self._abrotateTransform.angle = float(value) * 180/3.141592
        return locals()        


def loadLimbs():
    """ load the limbs from a file.
    """
    
    # Load struct
    fname = 'limbDataCoarse.ssdf'
    s = ssdf.load(fname)
    
    limbs = []
    
    for name in ['uleg', 'lleg', 'foot', 'toes']:
        
        # Get substruct for this limb
        sub = s[name]
        
        # Create mesh
        m = vv.BaseMesh(sub.vertices, sub.faces, sub.normals)
        m._limbLength = sub.length
        limbs.append(m)
    
    return tuple(limbs)


## Functions and definitions to produce a mesh


class LimbMeasure:
    """ Defines the dimensions of a limb.
    """
    def __init__(self, length, d1, d2):
        self.length = length
        self.d1 = d1
        self.d2 = d2

# Define limb sizes in mm: 
if True:  # Almar's legs
    BODY_MEASURE = LimbMeasure(500, vv.Point(160,450), vv.Point(150, 350))
    #
    ULEG_MEASURE = LimbMeasure(430, vv.Point(170, 160), vv.Point(120, 120))
    LLEG_MEASURE = LimbMeasure(450, vv.Point(120, 100), vv.Point(70, 70))
    FOOT_MEASURE = LimbMeasure(150, vv.Point(70, 70), vv.Point(40, 80))
    TOES_MEASURE = LimbMeasure(60,  vv.Point(30, 80), vv.Point(25, 70))
else:
    pass
    # todo: get measures from an anatomy book of average human



def getSphere(ndiv=3, radius=1.0):
    """ Creates a sphere by subdeviding a tetrahedon
    Example taken from the Red book, end of chaper 2.
    """
    radiusx, radiusy = normalizeRadia(radius)
    
    # Define constants
    X = 0.525731112119133606 
    Z = 0.850650808352039932
    
    # Creta vdata
    vdata = Pointset(3)
    app = vdata.append
    app(-X, 0.0, Z); app(X, 0.0, Z); app(-X, 0.0, -Z); app(X, 0.0, -Z)
    app(0.0, Z, X); app(0.0, Z, -X); app(0.0, -Z, X); app(0.0, -Z, -X)
    app(Z, X, 0.0); app(-Z, X, 0.0); app(Z, -X, 0.0); app(-Z, -X, 0.0)
    
    # Create faces
    tindices = [
        [0,4,1], [0,9,4], [9,5,4], [4,5,8], [4,8,1],    
        [8,10,1], [8,3,10], [5,3,8], [5,2,3], [2,7,3],    
        [7,10,3], [7,6,10], [7,11,6], [11,0,6], [0,1,6], 
        [6,1,10], [9,0,11], [9,11,2], [9,2,5], [7,2,11] ]
    tindices = np.array(tindices, dtype=np.uint32)
    
    # Init vertex array with existing points, init faces as empty list
    vertices = vdata.copy()
    faces = []
    
    # Define function to recursively create vertices and normals
    def drawtri(ia, ib, ic, div):
        a, b, c = vertices[ia] , vertices[ib], vertices[ic]
        if (div<=0):
            # Store faces here
            faces.extend([ia, ib, ic])
        else:
            # Create new points
            ab = Point(0,0,0)
            ac = Point(0,0,0)
            bc = Point(0,0,0)
            for i in range(3):
                ab[i]=(a[i]+b[i])/2.0;
                ac[i]=(a[i]+c[i])/2.0;
                bc[i]=(b[i]+c[i])/2.0;
            ab = ab.normalize(); ac = ac.normalize(); bc = bc.normalize()
            # Add new points
            i_offset = len(vertices)
            vertices.append(ab)
            vertices.append(ac)
            vertices.append(bc)
            iab, iac, ibc = i_offset+0, i_offset+1, i_offset+2
            #
            drawtri(ia, iab, iac, div-1)
            drawtri(ib, ibc, iab, div-1)
            drawtri(ic, iac, ibc, div-1)
            drawtri(iab, ibc, iac, div-1)
    
    # Create vertices
    for i in range(20):
        drawtri(    int(tindices[i][0]), 
                    int(tindices[i][1]), 
                    int(tindices[i][2]), 
                    ndiv )
    
    # Create normals and scale vertices
    normals = vertices.copy()
    vertices[:,0] *= radiusx
    vertices[:,1] *= radiusy
    vertices[:,2] *= radiusx
    
    # Create faces
    faces = np.array(faces, dtype='uint32')
    
    # Done
    return vertices, faces, normals


def getSemiCone(length, w1, w2, N=16, M=16):
    """ Create a semi cone, the radius for the two ends should be given.
    Both can be a tuple or Point to specify multiple radia (i.e. create 
    a cone with an ellipsoid transsection).
    """ 
    w1x, w1y = normalizeRadia(w1)
    w2x, w2y = normalizeRadia(w2)
    
    # Quick access
    pi2 = np.pi*2
    cos = np.cos
    sin = np.sin
    sl = N+1
    
    # Precalculate z component of the normal
    ww1, ww2 = 0.5*(w1x+w1y), 0.5*(w2x+w2y)
    zn = float(ww1-ww2)/length
    
    # Calculate vertices, normals and texcords
    vertices = Pointset(3)
    normals = Pointset(3)
    # Cone
    for m in range(M+1):
        factor = 1.0 - float(m)/M # between 0 and 1
        z = factor * length
        #
        for n in range(N+1):
            b = pi2 * float(n + 0.5*m) / N  # The 0.5*m is to make nicer triangles
            ax, ay = cos(b), sin(b)
            x = ax * ((1.0-factor) * abs(w1x-w2x) + min(w1x, w2x))
            y = ay * ((1.0-factor) * abs(w1y-w2y) + min(w1y, w2y))
            vertices.append(x,y,z)
            normals.append(ax,ay,zn)
    
    # Calculate indices
    indices = []
    for j in range(M):
        for i in range(N):
            #indices.extend([(j+1)*sl+i, (j+1)*sl+i+1, j*sl+i+1, j*sl+i])
            indices.extend([(j+1)*sl+i, (j+1)*sl+i+1, j*sl+i+1])
            indices.extend([(j+1)*sl+i, j*sl+i+1, j*sl+i])
    
    # Make indices a numpy array
    indices = np.array(indices, dtype=np.uint32)
    
    # Normalize normals
    normals = normals.normalize()
    
    # Flip
    vertices[:,2] *= -1.0
    
    # Change vertices to put 
    return vertices, indices, normals


def createLimb(limbMeasure, prevMeasure=None, close=False):
    """ Create a limb. This function creates a semicone and one or 
    two spheres for the joints. It needs the previous measure to
    calculate the best radius for the joint.
    """ 
    # Define some parameters
    ndiv = 1
    ncirc = 12
    jointExtra = 1.1
    
    # Get radius of first joint 
    r1 = r2 = Point(*normalizeRadia(limbMeasure.d1))
    if prevMeasure is not None:
        r2 = Point(*normalizeRadia(prevMeasure.d2))
    r1 = Point(max(r1.x, r2.x), max(r1.y, r2.y))
    rj1 = r1 * 0.5 * jointExtra
    
    # Get radius of second joint 
    r2 = Point(*normalizeRadia(limbMeasure.d2))
    rj2 = r2 * 0.5 * jointExtra
    
    # Init meshes list
    mm = []
    
    # Create first joint
    m1 = vv.BaseMesh(*getSphere(ndiv,  rj1))
    mm.append(m1)
    
    # Create semi cone for the limb
    m2 = vv.BaseMesh(*getSemiCone(limbMeasure.length, 
                        limbMeasure.d1*0.5, limbMeasure.d2*0.5,
                        ncirc, 8))
    mm.append(m2)
    
    # Create last limb?
    if close:
        m3 = vv.BaseMesh(*getSphere(ndiv, rj2))
        m3._vertices[:,2] -= limbMeasure.length
        mm.append(m3)
    
    # Combine and return (attach limbMeasure info)
    m = vv.processing.combineMeshes(mm)
    m._limbMeasure = limbMeasure
    return m


def normalizeRadia(r):
    """ Given a radius, this function always returns a tuple of two radia.
    """
    if isinstance(r, (float, int)):
        rx, ry = float(r), float(r)
    elif isinstance(r, tuple):
        rx, ry = r
    elif isinstance(r, vv.Point):
        rx, ry = r.x, r.y
    else:
        raise ValueError('Invalid value radius.')
    return rx, ry


def saveLimbs(leg, fileName='limbData.ssdf'):
    """ Save the mesh data of all limbs to an ssdf file.
    """
    s = ssdf.new()
    
    for name in ['uleg', 'lleg', 'foot', 'toes']:
        
        # Get mesh and substruct for this limb
        m = getattr(leg, '_'+name)
        s[name] = sub = ssdf.new()
        
        # Store mesh data
        sub.vertices = m._vertices
        sub.faces = m._faces
        sub.normals = m._normals
        sub.length = m._limbMeasure.length # We need the length
    
    ssdf.save(fileName, s)

class UseHumanWalkModel:
    def __init__(self, showFeet):
        """Class that uses the human walk model"""
        self._showFeet = showFeet
        self.F = vv.figure()
        self.A = vv.gca()
        self.human = HumanWalkModel(self.A, self._showFeet)
        self.human.PrepareAxes()
        self.human.UseLeftAndRight()
        self.A.camera.SetViewParams({'loc': (62, -86, 832), 'fov': 9.675, 'elevation': 14.025, 'zoom': 0.00056, 'daspect': (1.0, 1.0, 1.0), 'azimuth': 25.3, 'roll': -1.0})
        self.A.axis.visible=False
        
    def Demo(self):
        """Runs the demo"""
        fun = self.human.Demo()
        timeout = time.time() + 10
        while time.time() < timeout:
            fun()
            
    def Update(self,jointAngles,treadmillDistance=None):
        """Updates the pose of the model. Jointangles is a list, being:
            [ PX, PZ, LHA, LHF, LKF, LAP, RHA, RHF, RKF, RAP, PY, PRY, PRX, LFRY, RFRY]
            (note that some of these are ignored in the calculations)
        """
        self.human.anatomicReferenceX = jointAngles[0] * 1000
        self.human.anatomicReferenceY = jointAngles[10] * 1000
        self.human.anatomicReferenceZ = jointAngles[1] * 1000
        self.human.pelvisAxialRotation = jointAngles[11]
        self.human.pelvisObliquity = jointAngles[12]
        self.human.lega.hipAbductionAngle = jointAngles[2]
        self.human.lega.hipFlexionAngle = jointAngles[3]
        self.human.lega.kneeAngle = jointAngles[4]
        self.human.lega.ankleAngle = jointAngles[5]
        self.human.lega.ankleEndorotationAngle = -jointAngles[13]
        self.human.legb.hipAbductionAngle = jointAngles[6]
        self.human.legb.hipFlexionAngle = jointAngles[7]
        self.human.legb.kneeAngle = jointAngles[8]
        self.human.legb.ankleAngle = jointAngles[9]
        self.human.legb.ankleEndorotationAngle = jointAngles[14]
        
        if treadmillDistance is not None:
            self.human._treadmill.setDistance(treadmillDistance)
            
## Helper functions
def PlotFPSGraph(hwm):
    """When using the LLC_Emulator, you can call this function to plot a graph which shows
    the actual fps of the animation. hwm should point to the UseHumanWalkModel class, i.e.:
    from HumanWalkModel import PlotFPSGraph
    PlotFPSGraph(gui.llc.HWM)
    """
    import time
    t0=time.time()
    vv.figure(2); vv.clf()
    t = hwm.human._redrawTimes[:]
    tt=[x-t0 for x in t]
    dt = [ t[i+1]-t[i] for i in range(len(t)-1)]
    fps = [1/x for x in dt]
    vv.figure(2)
    vv.plot(tt[0:-1],fps,lc='b')
    
## Main class
if __name__ == '__main__':
    x=UseHumanWalkModel()
    #x.Demo()
    