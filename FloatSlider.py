from PySide import QtGui, QtCore

class FloatSlider(QtGui.QSlider):
    """Slider which allows the values to be in floats instead of ints.
    On top of that, it supports setting a snap interval. Default for snapping
    is a very small interval (such that the screen resolution is always more
    coarse that the interval). The default range for the slider is 0.0..1.0.
    
    Note that not all functions are overridden; using non-overridden functions
    may cause unexpected behavior.
    """
    valueChanged2 = QtCore.Signal(float) # this will be the new valueChanged signal (an alias will be made in __init__)
    
    def __init__(self,*args,**kwargs):
        QtGui.QSlider.__init__(self,*args,**kwargs)
        self._minimum = 0.0
        self._desiredMaximum = 1.0 # The actual maximum is dependent on the snap interval
        self._snapInterval = None
        self._previousValue = 0
        self._inhibitValueChangedEmit = False # Set to True to in
        
        # Override valueChanged signal
        self.baseValueChanged = self.valueChanged
        self.valueChanged = self.valueChanged2
        self.baseValueChanged.connect(self._conditionallyEmitValueChanged)

        self._update()
        
            
    # Getters
    def minimum(self):
        return self._minimum
    def maximum(self):
        return self._maximum
    def sliderPosition(self):
        return self._minimum + self._normalizedValueSliderPosition() * self._range
    def value(self):
        return self._minimum + self.normalizedValue() * self._range
    def snapInterval(self):
        return self._snapInterval

    def normalizedValue(self, v=None):
        """Returns the value of the slider as a float between 0.0 and 1.0.
        Alternatively, if v is given in the range self.minimum()..self.maximum()
        (i.e., the FloatSlider range), it returns the value v on the slider range
        as a float between 0.0 and 1.0"""
        if self._range > 0: # Normal case
            if v is None:
                v = QtGui.QSlider.value(self)
                mn = QtGui.QSlider.minimum(self)
                mx = QtGui.QSlider.maximum(self)
                return (v-mn)/(mx-mn)
            else:
                return (v-self._minimum) / self._range
        else: # Wrong range; self._minimum is only possible value
            return 0
            
    # Setters
    def setMinimum(self,v):
        currentValue = self.value()
        self._minimum = v
        if self._desiredMaximum < v:
            self._desiredMaximum = v
        self._update(currentValue)
        
    def setMaximum(self,v):
        currentValue = self.value()
        self._desiredMaximum = v
        if self._minimum>v:
            self._minimum = v
        self._update(currentValue)
        
    def setRange(self,minv,maxv):
        
        currentValue = self.value()
        self._minimum = minv
        self._desiredMaximum = max([minv,maxv])
        self._update(currentValue)
        
    def setSliderPosition(self,v):
        if self._range > 0: # Normal case
            v_normalized = (v-self._minimum)/self._range
        else: # Wrong range, self._minimum is only valid value
            v_normalized = 0
        mn_int = QtGui.QSlider.minimum(self)
        mx_int = QtGui.QSlider.maximum(self)
        v_int = round(mn_int + v_normalized * (mx_int-mn_int))
        QtGui.QSlider.setSliderPosition(self, v_int)
        
    def setValue(self,v):
        """Sets the slider to v. If snapping is enabled, it sets it to the 
        snapping position nearest to v."""
        if self._range>0: # Normal case
            v_normalized = (v-self._minimum)/self._range
        else: # Wrong range; self._minimum is only valid value
            v_normalized = 0
            
        mn_int = QtGui.QSlider.minimum(self)
        mx_int = QtGui.QSlider.maximum(self)
        v_int = round(mn_int + v_normalized * (mx_int-mn_int))
        QtGui.QSlider.setValue(self, v_int)

    def setSnapInterval(self,interval):
        """Set snapping. Set to None to disable snapping. Snap positions are
        calculated as (minimum + k * snapInterval).
    
        If (maximum-minimum) is not a multiple of snapInterval, then maximum is 
        adjusted towards the nearest snap position."""
        
        if interval is not None and interval <= 0:
            raise ValueError("Snap interval should be larger than zero.")
        currentValue = self.value()
        self._snapInterval = interval
        self._update(currentValue)

    # Private methods
    def _update(self,currentValue=None):
        """Given current values for self._minimum, self._maximum and self._snap,
        it updates the (integer) values of the QtGui.QSlider."""
        
        # When we're here, the calling function should have made sure that the assertion is valid
        assert self._minimum <= self._desiredMaximum
        
        self._inhibitValueChangedEmit = True
        if self._snapInterval is None: # No snapping
            self._maximum = self._desiredMaximum
            self._range = self._maximum-self._minimum
            if self._range > 0: # Normal case
                mx = 10000
                QtGui.QSlider.setMinimum(self,0)
                QtGui.QSlider.setMaximum(self, mx)
                QtGui.QSlider.setPageStep(self, mx/10)
                QtGui.QSlider.setSingleStep(self, mx/50)
            else: # Wrong range; self._minimum is only possible value. 
                # Make sure that there's only one possible value by setting the 
                # base-slider's minimum and maximum to the same value
                QtGui.QSlider.setMinimum(self,0)
                QtGui.QSlider.setMaximum(self, 0)
        else:
            nSteps = round((self._desiredMaximum-self._minimum) / self._snapInterval)
            self._maximum = self._minimum + nSteps*self._snapInterval
            self._range = self._maximum-self._minimum
            QtGui.QSlider.setMinimum(self,0)
            QtGui.QSlider.setMaximum(self, nSteps)
            QtGui.QSlider.setPageStep(self,1)
            QtGui.QSlider.setSingleStep(self,1)
            
        if currentValue is not None:
            self.setValue(currentValue)
            
        self._inhibitValueChangedEmit = False
        self._conditionallyEmitValueChanged()
        
            
    def _normalizedValueSliderPosition(self):
        """Returns the value of the slider as a float between 0.0 and 1.0."""
        if self._range > 0:  # Normal case
            v = QtGui.QSlider.sliderPosition(self)
            mn = QtGui.QSlider.minimum(self)
            mx = QtGui.QSlider.maximum(self)
            return (v-mn)/(mx-mn)
        else: # Wrong range; self._minimum is only possible value
            return 0
        
    def _conditionallyEmitValueChanged(self,v = None):
        """Checks if the value of the slider was changed; if so, it
        emits a valueChanged signal."""
        # v is sent by the base.valueChanged, but this value is 
        # not useful so we ignore it.
        if not self._inhibitValueChangedEmit:
            newValue = self.value()
            if self._previousValue != newValue:
                self._previousValue = newValue
                self.valueChanged.emit( self.value())
            
#
if __name__ == '__main__':
    s = FloatSlider()
#     s = QtGui.QSlidser()
    s.show()
    s.setGeometry(QtCore.QRect(183, 230, 142, 203))
    d = QtGui.QLabel()
    d.show()
    d.setGeometry(QtCore.QRect(195, 159, 116, 27))
    s.valueChanged.connect(lambda x:d.setText(str(x)))
    s.valueChanged.connect(lambda x:print("value changed to",str(x)))