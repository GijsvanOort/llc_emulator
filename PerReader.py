# PerReader.py
# Functions to read data from the PER.XML file
import xml.etree.ElementTree as ET

class PerReader:
    # JointInfo: Each Joint name points to a tuple (index, multiply); where
    # - index is the index in strings like 'set x pos [x,x,x,x,x,..,x]'
    # - and multiply= -1 means that the definition of positive in the LLC (i.e,
    #   cw around axis when you look into the positive direction of the axis) and
    #   in anatomic (i.e., positive if you go into the direction indicated by the
    #   abbreviation) is opposite.
    JointInfo = { \
        'PX':  (0,  1), 
        'PZ':  (1,  1), 
        'LHA': (2,  1), 
        'LHF': (3,  1), 
        'LKF': (4, -1), 
        'LAP': (5, -1),
        'RHA': (6, -1),
        'RHF': (7,  1), 
        'RKF': (8, -1),
        'RAP': (9, -1)} 
    
    def __init__(self, fileName='per.xml'):
        self._fileName = fileName
        self._xmlTree=ET.parse(fileName)
        
    def GetJointRangeAnatomic(self,jointName):
        """Returns as list [min, max] the range of the joint (in anatomic references). No error checking in here."""
        i, m = self.JointInfo[jointName]
        
        JointLimiter = self._xmlTree.find(".//Node[@Name='PVAHumanJointLimiter%d']"%i)
        
        v1 = m * float(JointLimiter.find("./Property[@Name='LowEndStop']").attrib['Value'])
        v2 = m * float(JointLimiter.find("./Property[@Name='HighEndStop']").attrib['Value'])
        return sorted([v1,v2])

    def GetJointRange(self,jointName):
        """Returns as tuple (min, max) the range of the joint (in LLC references). No error checking in here."""
        i, m = self.JointInfo[jointName]
        
        JointLimiter = self._xmlTree.find(".//Node[@Name='PVAHumanJointLimiter%d']"%i)
        
        v1 = float(JointLimiter.find("./Property[@Name='LowEndStop']").attrib['Value'])
        v2 = float(JointLimiter.find("./Property[@Name='HighEndStop']").attrib['Value'])
        return (v1,v2)

    def GetJointRanges(self):
        """Returns the ranges of all joints as one list of tuples."""
        return [self.GetJointRange(x) for x in ['PX','PZ','LHA','LHF','LKF','LAP','RHA','RHF','RKF','RAP']]
    
    def GetSegmentInertias(self):
        """Returns a list of the SegmentInertias."""
        return [float(self._xmlTree.find(".//Node[@Name='Models']/Property[@Name='Inertia%d']"%i).attrib['Value']) for i in range(10)]
        
    def GetCartesianInertias(self):
        """Returns a list of the CartesianInertias."""
        return [float(self._xmlTree.find(".//Node[@Name='Models']/Property[@Name='CartesianInertia%d']"%i).attrib['Value']) for i in range(10)]
        
        
if __name__=='__main__':
    # Test
    P=PerReader()
    for x in ['PX','PZ','LHA','LHF','LKF','LAP','RHA','RHF','RKF','RAP']:
        print(x,P.GetJointRangeAnatomic(x))
    print('JointRanges:      ',P.GetJointRanges())
    print('Segmentinertia:   ',P.GetSegmentInertias())
    print('CartesianInertia: ',P.GetCartesianInertias())