## DebugWidget
from PySide import QtGui,QtCore

class DebugWidget(QtGui.QTextEdit):
    def __init__(self,*args,**kwargs):
        QtGui.QTextEdit.__init__(self,*args,**kwargs)
        self.setObjectName('DebugWidget')
        self.setFont(QtGui.QFont('courier',11))
        self.setReadOnly(True)
        self.append("Debug Log");
        
        # Add a right-mousebutton menu for clearing the log
        self.MyMenu = QtGui.QMenu()
        self.MyMenu.addAction("Clear",self.clear)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(lambda point:self.MyMenu.exec_(self.viewport().mapToGlobal(point)))