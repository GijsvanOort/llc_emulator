## General-purpose widgets
from PySide import QtCore, QtGui

class LineEditWithHistory(QtGui.QLineEdit):
    """Advanced Line Edit with history.
    Up and down arrows cycle through all history elements that start with 
    the characters you typed in."""
    enterPressed = QtCore.Signal(str)
    
    def __init__(self):
        QtGui.QLineEdit.__init__(self)
        self.history = []
        self.historyIndex = -1 # -1: no history line selected
        self.setFont(QtGui.QFont('courier',11))
        self.setModified(True)
        
    def addToHistory(self,s,force=False):
        """If force==True, it will be added to history even if it was already in the history"""
        if s.strip() is not "" and ((s not in self.history) or force):
            self.history.append(s)

    def selectHistory(self,direction):
        if self.isModified():
            # Compile a list of all history lines starting with the current line
            self._historySubset = [s for s in self.history if s.startswith(self.text())]
            # ... and select the appropriate line
            self._historySubsetIndex = 0 if direction==1 else len(self._historySubset)-1
        else:
            if len(self._historySubset)==0:
                # No lines, so nothing to do
                return
                
            # then the historysubset already exists, so just select a different line.
            # (note that we can only come here if the list is non-empty.
            self._historySubsetIndex = (self._historySubsetIndex + direction) % len(self._historySubset)

        if len(self._historySubset)==0:
            # Then there are no lines starting with the string, so don't do anything.
            self.setModified(False)
        else:
            self.setText(self._historySubset[self._historySubsetIndex])
            self.setModified(False)
            self.setCursorPosition(len(self.text()))

        
    # Overload the keyPressEvent
    def keyPressEvent(self,event):
        if event.key() in [QtCore.Qt.Key_Enter, QtCore.Qt.Key_Return]:
            self.enterPressed.emit(self.text())
            self.addToHistory(self.text())
            self.setText('')
            self.setModified(True)
        elif event.key() == QtCore.Qt.Key_Escape:
            self.setText('')
            self.setModified(True)
        elif event.key() == QtCore.Qt.Key_Up:
            self.selectHistory(-1)
        elif event.key() == QtCore.Qt.Key_Down:
            self.selectHistory(+1)
        else:
            # modified is automatically set to true in this case
            self.historyIndex = -1
            QtGui.QLineEdit.keyPressEvent(self,event)
            
    def showHistory(self):
        """For debugging"""
        print (("" if self.isModified() else "Not"),"modified; ",self.historyIndex)
        for x in enumerate(self.history):
            st = "*" if self.historyIndex==x[0] else " "
            print (st,x[1])
            
if __name__=='__main__':
    # Test lineEdit.
    L = LineEditWithHistory()
    L.enterPressed.connect(print)
    L.show()