## LLC Emulator
#
# This script acts as if it is the LLC; it sends commands like the LLC will
# do in the future.
from PySide.QtCore import QThread, QObject,QMutex
from PySide import *
import socket
import time
import re
import random
import numpy
import visvis as vv
from pprint import pprint as pp
from LowLevelController import *
from GeneralPurposeWidgets import LineEditWithHistory
from DebugWidget import DebugWidget
from ForceInteractionWidget import ForceInteractionWidget
from TrajectoryWidget import TrajectoryWidget
dataMutex = QMutex()


class Menu:
    def __init__(self, llc, printFcn = print):
        self.llc = llc
        
        # Set print function to default (you can override that easily by for example doing self.printFcn=myPrintFunction)
        self.printFcn = printFcn
        
    def _toggle(self,measurementKey, printMessage=True):
        self.llc.measurements[measurementKey] = not self.llc.measurements[measurementKey]
        self.printFcn("Set %s to %s."%(measurementKey, "True" if self.llc.measurements[measurementKey] else "False"))

    def PrintHelp(self):
        self.printFcn ("""Examples:
        d2     - set debug level to 2
                 debugLevel 0: no printing at all
                 debugLevel 1: prints warnings and errors
                 debugLevel 2: also prints received and transmitted messages
                 debugLevel 3: also prints each poll, even if no message was received/transmitted.
        d2 25001 - set debug level only for channel involving port 25001
        c get state - returns the state  (c set state off - sets state etc)
        cget state - same as above
        """)
        
    def PrintMenu(self):
        def OffOn(b): return '(is now on)' if b else '(is now off)'
        def OO(b): return 'on' if b else 'off'
        def FalseTrue(b): return '(is now true)' if b else '(is now false)'
        def ReleasedPressed(b): return '(is now released)' if b else '(is now pressed)'
        
        """Prints the menu in which you can toggle things. Returns True if we need to quit"""
        self.printFcn ("\n=== "+time.strftime("%H:%M:%S")+" ===============================================================")
        self.printFcn ("INFO     Current state: '%s'"%self.llc.state)
        self.printFcn ("         Messages sent: " + "; ".join(["ch%d: %d"%((n+1),self.llc._x[n].nMessagesSent) for n in range(len(self.llc._x))]+["err: %d"%self.llc._err.nMessagesSent]))
        self.printFcn ("TOGGLES  1. Communication %s"%[OO(x.isOn) for x in self.llc._x])
        
        self.printFcn ("         2. Initialized %s"%FalseTrue(self.llc.measurements['isinitok']))
        self.printFcn ("         3. Emergency button / CabinetBlueButtonPressed %s"%FalseTrue(self.llc.measurements['iscabinetbluebuttonpressed']))
        self.printFcn ("         4. isPatientDefined %s"%FalseTrue(self.llc.measurements['ispatientdefined']))
        self.printFcn ("         5. isForceCalibrated %s"%FalseTrue(self.llc.measurements['isforcecalibrated']))
        self.printFcn ("         6. isMotorsOk %s"%FalseTrue(self.llc.measurements['ismotorsok']))
        self.printFcn ("         7. isTreadmillEmpty %s"%FalseTrue(self.llc.measurements['istreadmillempty']))
        self.printFcn ("         8. isReadyForOperationall %s"%FalseTrue(self.llc.measurements['isreadyforoperational']))
        self.printFcn ("COMMANDS d. Set Debug level (is now %s)"%[x.GetDebugLevel() for x in (self.llc._x+[self.llc._err])])
        self.printFcn ("         c. Send single command to communication port (and get answer)")
        self.printFcn ("         n. Set noise amount (0 is nothing, 1 is a lot); (is now %f)"%self.llc.noiseAmount)
        self.printFcn ("         r. Hard-reset the LLC")
        self.printFcn ("         k. Set step time of dynamics integration (is now %f s)"%self.llc._integrationStepTime)
        self.printFcn ("         e. Show list of all effects")
        self.printFcn ("         i. Set HLC IP (for Error Channel) (is now %s)"%self.llc.hlcIP)
        self.printFcn ("         m. Send error message through Error Channel")
        self.printFcn ("         t. Set Treadmill Fy value (is now %f)"%self.llc.properties['treadmill'][6])
        self.printFcn ("         u. Toggle dynamics %s"%OffOn(self.llc._updateDynamics))
        watchdogString = "" if not self.llc._watchdogEnabled else "(timeout is %d ms%s)"%(self.llc.properties["watchdogtimeout"],"" if self.llc.properties["watchdogtimeout"]!=0 else " -> never times out")
        self.printFcn ("         w. Force watchdog (is now %s) %s"%(OO(self.llc._watchdogEnabled),watchdogString))
        self.printFcn ("         f. Set fps (is now %d)"%self.llc.fps)
        self.printFcn ("         p. Have patient in Lopes (alters masses) %s"%FalseTrue(self.llc.patientInLopes))
        self.printFcn ("         ?. This menu")
        self.printFcn ("         ??. Extra help")
        self.printFcn ("         q. Quit")
        self.printFcn ("         qs. Quit and stop LLC emulator")
        self.printFcn ("")
            
    def ParseInput(self,z):
        """Parses the user input. Returns True if we should quit the main loop, False otherwise"""
        if (len(z)==0):
            pass # Do nothing
        elif (z[0]=='1'):
            if len(z)>1:
                try:
                    args = z[1:].split()
                    args_int = [int(x) for x in args]
                    def getChannel(port):
                        """Returns a reference to the channel given the port number, or None if it doesn't exist"""
                        xx= [x for x in self.llc._x if (x.recvPort == port or x.sendPort == port)]
                        return xx if len(xx)!=0 else None
                        
                    u = [u for u in [getChannel(p) for p in args_int if p] if u is not None]
                    self.printFcn(u)
                    if len(u) != 0:
                        for xx in u:
                            self.llc.SetCommunication('toggle',xx)
                    else:
                        self.printFcn("No communication channel involving port %d"%args_int[1])            
                except ValueError as e:
                    self.printFcn("Invalid arguments for commmunication(error %s)"%e)
            else:   
                self.llc.SetCommunication('toggle')
            self.printFcn("Set communciation to %s"%[OO(x.isOn) for x in self.llc._x])
        elif (z=='2'):
            self._toggle('isinitok')
            if  self.llc.measurements['isinitok']:
                self.llc.measurements['ismotorsok'] = True
                self.printFcn("Set ismotorsok to True because isInitOk being True implies that isMotorsOk is also True.")
        elif (z=='3'):
            self._toggle('iscabinetbluebuttonpressed')
        elif (z=='4'):
            self._toggle('ispatientdefined')
        elif (z=='5'):
            self._toggle('isforcecalibrated')
        elif (z=='6'):
            self._toggle('ismotorsok')
            if not self.llc.measurements['ismotorsok']:
                self.llc.measurements['isinitok'] = False
                self.printFcn("Set isinitok to False because isMotorsOk being false implies that isInitOk is also false.")
        elif (z=='7'):
            self._toggle('istreadmillempty')
        elif (z=='8'):
            self._toggle('isreadyforoperational')
        elif (z[0]=='d'):
            try:
                args = z[1:].split()
                args_int = [int(x) for x in args]
            except ValueError as e:
                self.printFcn("Invalid arguments for debug level (error %s)"%e)
                
            if len(args)==1:
                for xx in self.llc._x+[self.llc._err]:
                    xx.SetDebugLevel(args_int[0])
            elif len(args)==2:
                u = [x for x in self.llc._x if (x.recvPort == args_int[1] or x.sendPort == args_int[1])]
                if len(u) != 0:
                    for xx in u:
                        xx.SetDebugLevel(args_int[0])
                elif self.llc._err.sendPort == args_int[1]: # Error Channel matches
                    self.llc._err.SetDebugLevel(args_int[0])
                else:
                    self.printFcn("No communication channel involving port %d"%args_int[1])
        elif (z[0]=='t'):
            try:
                x = float(z[1:])
                self.llc.properties['treadmill'][6] = x
            except ValueError as e:
                self.printFcn("Invalid value for treadmill Fy (error: %s)"%e)
        elif (z[0]=='c'):
            commandString = z[1:].lstrip()
            commands = [c.strip() for c in commandString.split(";")]
            answer = ";".join([self.llc._x[0].ParseCommand(c) for c in commands])
            self.printFcn("Command: %s\nAnswer:  %s"%(commandString,answer))
        elif z[0]=='e':
            self.printFcn("List of effects currently available:")
            if len(self.llc.effects)==0:
                self.printFcn("      None")
            else:
                for x in self.llc.effects.keys():
                    effect = self.llc.effects[x]
                    self.printFcn( "%15s: %s (%s)"%(effect["_type"],x, "enabled" if effect["enabled"] else "disabled"))
        elif z[0]=='f':
             x = int(z[1:])
             self.llc.SetFps(x)
             self.printFcn ("Set fps to %d"%x)
        elif z=='p':
            if self.llc.patientInLopes:
                self.llc.patientInLopes = False
                self.printFcn("Patient was removed from lopes (inertia is only virtual robot inertia)")
            else:
                self.llc.patientInLopes = False
                self.printFcn("Patient was put in lopes (inertia is virtual robot inertia plus patient inertia)")
        elif (z.startswith('??')):
            self.PrintHelp()
        elif (z=='?'):
            self.PrintMenu()
        elif (z=='r'):
            self.printFcn ("Resetting the LLC")
            self.llc.HardReset()
        elif (z[0]=='n'):
            try:
                x = float(z[1:])
                llc.setNoiseAmount(x)
            except ValueError as e:
                self.printFcn("Invalid noise amount (error: %s)"%e)
        elif z.startswith('i'):
            ip = z[1:].strip()
            self.llc.SetHLCIp(ip)
        elif z.startswith('m'):
            message = z[1:].strip()
            self.llc._err.Send(message)
        elif z.startswith('u'):
            self.llc.SetUpdateDynamics()
            self.printFcn ("Updating of dynamics is now %s"%("on" if self.llc._updateDynamics else "off"))
        elif (z[0]=='k'):
            try:
                x = float(z[1:])
                self.llc.SetIntegrationStepTime(x)
            except ValueError as e:
                self.printFcn("Invalid value for maximum step time for dynamics integration (error: %s)"%e)
        elif (z[0]=='w'):
            self.printFcn("Watchdog is now %s"%("on" if self.llc.SetWatchdogEnabled('toggle') else "off"))
        elif (str.lower(z)=='q'):
            self.printFcn ("Quitting menu; LLC emulator still runs!")
            return True
        elif (str.lower(z)=='qs'):
            self.llc.Stop()
            self.printFcn ("Quit menu and stopped LLC emulator.")
            return True
        else:
            self.printFcn("Unknown command %s"%z)
            return False
        
    def Menu(self):
        
        
        z = input('<LLC> ')
        self.printFcn(z)
        return self.ParseInput(z)
                
    def Run(self):
        """Runs the menu infinitely, until quit"""
        quit = False
        while (not quit):
            quit =  self.Menu()
            time.sleep(0.2)
            


##         
class LLC_EmulatorGUI(QtGui.QWidget):
    def __init__(self, includeAnkles = True):
        QtGui.QWidget.__init__(self)
        
        self._includeAnkles = includeAnkles
        self._llcThread = QtCore.QThread()
        self.llc = LLC(hlcIP='192.168.56.53', includeAnkles = self._includeAnkles)
        self.llc.moveToThread(self._llcThread)
        self._llcThread.start(QtCore.QThread.Priority.HighPriority)
        self.llc._llcThread = self._llcThread # Link to the thread so that we can quit the thread from inside the llc class
        self.menu = Menu(self.llc, self.printToTextWidget)
        
        self.textWidget = QtGui.QTextEdit()
        self.textWidget.setFont(QtGui.QFont('courier',11))
        self.textWidget.setReadOnly(True)
        
        # Text widgets
        self.debugWidget = DebugWidget()
        
        self.lineWidget = LineEditWithHistory()
        self.lineWidget.addToHistory("c set state force;set segmentspring0 pos [0,0,0,0,-1,-1,0,0,0,0];set segmentspring0 stiffness [1,1,1,1,1,1,1,1,1,1];set segmentspring0 enable",force=True)
        self.lineWidget.addToHistory("c set jointspring0 stiffness [1,1,1,1];set jointspring0 maxforce [2,2,2,2];set jointspring0 dampfactor [0.01,0.01,0.01,0.01]; set jointspring0 enable",force=True)
        self.lineWidget.addToHistory("c set jointspring0 disable",force=True)
        self.lineWidget.addToHistory("c set state position;create spring a;set a stiffnesssegments [99,99,99,99,99,99,99,99,99,99];set a enable",force=True)
        self.lineWidget.addToHistory("c set state position;create spring b;set b stiffnessjoints [99,99,99,99,99,99,99,99,99,99];set b enable",force=True)
        self.lineWidget.addToHistory("c set state position;create spring b;set b stiffnessjoints [99,99,99,99,99,99,99,99,99,99];set b dampfactorjoints [0,0,0,0,0,0,0,0,0,0];set b enable",force=True)
        self.lineWidget.enterPressed.connect(self.ParseInputAndPrintMenu)
        
        self.graphWidget = self.llc.HWM.F._widget
        
        self.MenuDebugContainer = QtGui.QSplitter()
        self.MenuDebugContainer.setOrientation(QtCore.Qt.Vertical)
        self.MenuDebugContainer.addWidget(self.debugWidget)
        self.MenuDebugContainer.addWidget(self.textWidget)
        self.MenuDebugContainer.setSizes([1,4])
        self.MenuDebugContainer.setStretchFactor(0,0)
        self.MenuDebugContainer.setStretchFactor(1,1)
        
        # Force interaction
        # Add an effect for the interaction 
        self.InteractionWidget = ForceInteractionWidget( \
            lambda: JointPositionsToAnatomicPositions(SegmentPositionsToJointPositions(self.llc.measurements['segmentmodelpos'])), \
            lambda p,s: self.llc.SetExternalForceSpring(0, AnatomicPositionsToJointPositions(p),s), \
            self._includeAnkles)
        
        # TrajectoryFromFile
        self.TrajctoryWidget = TrajectoryWidget( \
            lambda: JointPositionsToAnatomicPositions(SegmentPositionsToJointPositions(self.llc.measurements['segmentmodelpos'])), \
            lambda p,s: self.llc.SetExternalForceSpring(1, AnatomicPositionsToJointPositions(p),s), \
            self._includeAnkles)
        
        self.leftWidget = QtGui.QTabWidget()
        self.leftWidget.addTab(self.MenuDebugContainer,'Menu/Debug')
        self.leftWidget.addTab(self.InteractionWidget,'Force Interaction')
        self.leftWidget.addTab(self.TrajctoryWidget,'Trajectory')
        
        
        vboxRight = QtGui.QVBoxLayout()
        self.stateLabel=QtGui.QComboBox()
        self.stateLabel.addItem('Here comes state information')
        for x in ['off','init','home','position','force']:
            self.stateLabel.addItem("Request state '%s'"%x)
        self.stateLabel.setMaximumHeight(30);
        self.stateLabel.setStyleSheet("font-size:14pt;text-align:left")
        self.stateLabel.currentIndexChanged.connect(self.RequestStateChange)
        
        self.nMessagesLabel = QtGui.QPushButton("Here comes the number of messages")
        self.nMessagesLabel.setMaximumHeight(55);
        self.nMessagesLabel.setStyleSheet("font-size:14pt;text-align:left")
        self.nMessagesLabel.clicked.connect(self.ResetNMessagesSent)
        vboxRight.addWidget(self.stateLabel)
        vboxRight.addWidget(self.nMessagesLabel)
        # Embed the graph in this window,with a bar above it to set the size (smaller size makes simulation more fluent)
        self.GraphSplitter = QtGui.QSplitter()
        self.GraphSplitter.setOrientation(QtCore.Qt.Vertical)
        self.InstructionLabel = QtGui.QLabel('Make animation window smaller to decrease processor load')
        self.InstructionLabel.setStyleSheet('background-color:white')
        self.GraphSplitter.addWidget(self.InstructionLabel)
        self.GraphSplitter.addWidget(self.graphWidget)
        vboxRight.addWidget(self.GraphSplitter)
        
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.leftWidget)
        hbox.addLayout(vboxRight) 
        
        
        vbox = QtGui.QVBoxLayout()
        vbox.addLayout(hbox)
        vbox.addWidget(self.lineWidget)
        
        hbox.setStretch(0,2)
        hbox.setStretch(1,1)
        self.graphWidget.setMinimumWidth(150)
        
        self.setLayout(vbox)
        
        # For 2-screens: self.mainWindow.setGeometry(QtCore.QRect(2012, 175, 274, 236))
        self.setGeometry(QtCore.QRect(346, 55, 1300, 500)) #Single-monitor
        self.showMaximized() 
        self.setWindowTitle('LLC Emulator')
        
        # Add a timer for updating the number of messages sent
        self.nMessagesLabelTimer = QtCore.QTimer()
        self.nMessagesLabelTimer.timeout.connect(self.UpdateMessagesSentLabelAndStateLabel)
        self.nMessagesLabelTimer.start(100)
        
        # Connect the print function to printing signals of other classes
        for l in self.llc._x + [self.llc._err]:
            l.debugPrintSignal.connect(self.debug_print)
        self.llc.myPrintSignal.connect(self.printToTextWidget)
        
        self.show()
        
    def SendCommandToLLC(self,s):
        commandString = s.lstrip()
        commands = [c.strip() for c in commandString.split(";")]
        answer = ";".join([self.llc._x[0].ParseCommand(c) for c in commands])
        return answer
        
    def ParseInputAndPrintMenu(self,input):
        self.printToTextWidget("<b>"+input)
        if self.menu.ParseInput(input):
            self.close()
    
    def UpdateMessagesSentLabelAndStateLabel(self):
        self.stateLabel.setItemText(0,"State: '%s'"%self.llc.state)
        
        n = [xx.nMessagesSent for xx in self.llc._x]
        s = ";  ".join(["ch%d: %3d"%(x[0]+1, x[1]) for x in enumerate(n)] + ["err: %3d"%self.llc._err.nMessagesSent])
        self.nMessagesLabel.setText("Messages sent:\n"+s)
        
    def RequestStateChange(self,v):
        if v==0:
            return
        states= [None, 'off','init','home','position','force']
        theState = states[v]
        self.printToTextWidget("Requesting state %s"%theState)
        self.llc.RequestStateChange(theState, force=True)
        self.stateLabel.setCurrentIndex(0)
        
    def ResetNMessagesSent(self):
        for x in self.llc._x:
            x.nMessagesSent=0
        self.llc._err.nMessagesSent = 0
        
    def printToTextWidget(self,*x):
        v = self.textWidget.verticalScrollBar()
        scrollToEnd=True if v.value() == v.maximum() else False
        self.textWidget.append(' '.join(x))
        if scrollToEnd:
            v.setValue(v.maximum())

    def debug_print(self,*x):
        self.debugWidget.append(' '.join(x))
    
##        
app = QtGui.QApplication([]) # Create application before creating anything else
gui = LLC_EmulatorGUI(includeAnkles=False)

gui.setGeometry(QtCore.QRect(848, 30, 824, 982)) # Right half of single-screen
# gui.setGeometry(QtCore.QRect(2032, 72, 1486, 879)) # Entire second screen
my_print = gui.printToTextWidget
debug_print = gui.debug_print
    
app.exec_()

