"""TRAJECTORYWIDGET.PY

A widget that allows to replay trajectories from file. They are replayed as if the
human exerts forces on the system:
If the system is in state off, it exerts them directly on the mechanics.
If the system is in state force, it exerts them to the force sensors.
Otherwise, the forces are ignored. See also ForceInteractionWidget and
the llc._externalForceSpringX variables.
"""
import numpy as np
import scipy.interpolate
import math
import csv
import os.path
from pyzolib import ssdf

from PySide import QtCore, QtGui
from FloatSlider import FloatSlider

class Trajectory:
    """Implements a trajectory. This class stores all the data and has a function
    to get the data at a certain time. Subclasses allow to read the trajectory from
    file or other ways to get the trajectory. The default is a one-second long zero-trajectory"""
    
    # Implementation of the trajectory: we have an array self._time and self._values of equal length.
    # Each element of self._values consists of an array of length self._nDofs.
    # In self._time (and thus the 'upper layer' of self._values) there should be at least two elements.
    # The first element in self._time should be zero.
    # If the values in self._time are not strictly monotonically increasing, the behaviour of this
    # class may be unexpected.
    def __init__(self, includeAnkles):
        # Initialize with default values
        self._includeAnkles = includeAnkles
        self._nDofs = 10 if self._includeAnkles else 8
        self._time = [0,1]
        self._values = 2* [ self._nDofs*[0] ]
        self._interpFunction = None
        
    def IsTrajectoryOk(self):
        """Does a series of simple checks to the trajectory, returns (True,'') if ok.
        If the trajectory is not ok, it returns (False, explanationString)."""
            
        # Length of elements
        if len(self._time) != len(self._values):
            return (False, "Time array and values array don't have the same length.")
        if len(self._time)<2:
            return (False, "Number of data points less than two.")
        if any( [len(x)!=self._nDofs for x in self._values] ):
            return (False, "At least for one point in time, the values array doesn't have %d values."%self._nDofs)
        if any(np.diff(self._time)<=0):
            return (False, "Time values not stictly increasing.")
        if self._time[0]!= 0:
            return (False, "Time array does not start at zero.")
        if any( [  any([not isinstance(v, (int,float)) for v in x]) for x in self._values]):
            return (False, "Some elements in self._values are not a number.")
        if any([any(np.isnan(v)) for v in self._values]):
            return (False, "Some elements in self._values are NaN.")
        return (True, "")
        
    def SetupInterpolationFunction(self):
        self._interpFunction = scipy.interpolate.interp1d(self._time, self._values, axis=0)
        
    def getDuration(self):
        return self._time[-1]
        
    def values(self,t):
        """Given t, it returns the values corresponding to that time. If 
        self._time[0]<=t<=self._time[end], linear interpolation is done. Otherwise,
        0th order extrapolation is done (i.e., it returns the zeroth or last value)."""
        if t<=self._time[0]:
            return self._values[0]
        elif t>=self._time[-1]:
            return self._values[-1]
        else:
            if self._interpFunction is None:
                self.SetupInterpolationFunction()
            return list(self._interpFunction(t))

class SineTrajectory(Trajectory):
    """Debug trajectory; creates sine waves for each DOF."""
    def __init__(self, includeAnkles, amplitudes):
        Trajectory.__init__(self, includeAnkles)
        
        if len(amplitudes)!= self._nDofs:
            return ValueError("parameter amplitudes should have %d elements, but has %d"%(self._nDofs, amplitudes))
            
        t_end = 4
        self._time = list(np.linspace(0,t_end))
        freqs = [2*np.pi/t_end* (1+i) for i in range(self._nDofs)]
        
        self._values = []
        for t in self._time:
            self._values.append( [a*math.sin(f*t) for (a,f) in zip(amplitudes,freqs)])
        
        isOk = self.IsTrajectoryOk()
        if isOk[0]:
            print("Trajectory passed the test.")
        else:
            print("Trajectory didn't pass the test: %s"%isOk[1])
        
class TrajectoryFromFile(Trajectory):
    """This class can read trajectory data from a file.
    The data should be in a comma/space/tab-delimited file (i.e., something which
    can be read with Python's csv-module).
    It should have a header row, indicating the joints. It can be either with or without
    ankle information. They MUST be (case insensitive, delimiters may be anything):
    Time, PX, PZ, LHA, LHF, LKF, RHA, RHF, RKF
    or
    Time, PX, PZ, LHA, LHF, LKF, LAP, RHA, RHF, RKF, RAP
    Any other header gives an error. If ankles are included, then only the latter is allowed.
    """
    def __init__(self, includeAnkles, fileName):
        Trajectory.__init__(self, includeAnkles)
        
        # Read the data from the file
        with open(fileName) as csvfile:
            dialect = csv.Sniffer().sniff(csvfile.read(1024))
            csvfile.seek(0)
            reader = csv.reader(csvfile, dialect)
            
            # now: the real processing
            headers = next(reader)
            time = []
            values=[]
            for fields in reader:
                time.append(float(fields[0]))
                values.append([float(x) for x in fields[1:]])
                
        # Now: check if we have good data
        if len(headers)==9:
            if includeAnkles:
                raise RuntimeError("File seems to have only 8 joints, not 10 (which is required for using with ankles).")
            headerTemplate ="time px pz lha lhf lkf rha rhf rkf".split(" ")
        elif len(headers)==11:
            headerTemplate ="time px pz lha lhf lkf lap rha rhf rkf rap".split(" ")
        else:
            raise RuntimeError("Expected 1+8 (no ankles) or 1+10 columns, but got %d columns"%len(headers))
            
        for i in range(len(headers)):
            if headers[i].lower()!= headerTemplate[i]:
                raise RuntimeError("Error in %dth element of header. Expected '%s' but got '%s'"%(i+1, headerTemplate[i], headers[i]))
                
        # If we're here, then the header is ok. Let's assume that the rest is also ok (we'll check later anyway)
        if len(headers)==10 and not includeAnkles:
            # Then we need to remove the ankle values from the list
            values = [ x[0:5]+x[6:9] for x in values]
            
        # Assign to the member variables and check if everything is correct.
        self._time = time
        self._values = values
        self.IsTrajectoryOk()
        self.SetupInterpolationFunction()
            
class TrajectoryFromSignalsFile(Trajectory):
    """Reads position data from a .signalsfile and stores it as a trajectory.
    The user is asked if he wants to use the measured or reference data."""
    def __init__(self, includeAnkles, fileName):
        Trajectory.__init__(self, includeAnkles)
   
        y = self.readSignalRecording(fileName)
        
        msgBox = QtGui.QMessageBox(QtGui.QMessageBox.Question,"Select data source","Should I use the reference positions, or the measured positions?")
        referenceButton = msgBox.addButton("Use reference positions", QtGui.QMessageBox.ActionRole)
        measuredButton = msgBox.addButton("Use measured positions", QtGui.QMessageBox.ActionRole)
        msgBox.exec_()
        
        if msgBox.clickedButton() == referenceButton:
            prefix = 'r.cont.jointAngles.'
        elif msgBox.clickedButton() == measuredButton:
            prefix = 'm.cont.jointAngles.'

        # Assign
        self._time = list ( y['time'] - y['time'][0] )
        
        if includeAnkles:
            joints = "PX PZ LHA LHF LKF LAP RHA RHF RKF RAP".split(' ')
        else:
            joints = "PX PZ LHA LHF LKF RHA RHF RKF".split(' ')
        
#         yy=( y[ prefix+j ] for j in joints)
        
        self._values = []
        for i in range(len(self._time)):
            vv = [ y[prefix+j][i] for j in joints ] 
            self._values.append(vv)
            
        
    def readSignalRecording(self,filename):
        """
        Read a signal recording. Returns a dict that maps signal keys to 1D numpy
        arrays.
        
        """
        
        # Open file
        f = open(filename, 'rb')
        
        # Define separtor
        t1 = ('='*40).encode('ascii')
        t2 = '\n'.encode('ascii')
        separatorPos = 0
        
        # Seek separator 
        while True:
            test = f.read(40)
            if not test:
                break
            elif test == t1:
                separatorPos = 0
                for i in range(41):
                    test = f.read(1)
                    if test == t1[:1]:
                        continue
                    elif test == t2:
                        separatorPos = f.tell()
                    else:
                        break
                if separatorPos:
                    break
        
        # Check
        if not separatorPos:
            raise RuntimeError('Could not find separator.')
        else:
            separatorPos1, separatorPos2 = separatorPos - 81, separatorPos
        
        # Read header
        f.seek(0)
        header = ssdf.loads(f.read(separatorPos1).decode('utf-8'))
        
        # Read remaining data
        f.seek(separatorPos2)
        data = f.read()
        f.close()
        
        # Check size that we need for arrays
        N = len(header.map)
        M = len(data) // (N*8)
        
        # Make numpy array
        a = np.frombuffer(data, np.float64, count=N*M)
        a.shape = M, N
        
        # Check what's left, and check last element
        numberOfExtraElements = len(data) - N*M*8
        lastCharIsLineFeed = data[-1:] == b'\n'
        if numberOfExtraElements != 1:
            print('Warning: Data looks incomplete: footer is not 1 element but %i' % numberOfExtraElements)
        elif not lastCharIsLineFeed:
            print('Warning: Data looks incomplete: footer is not a newline but %r' % data[-1:])
        
        # Make dict, return
        dataDict = header.__dict__
        for i, key in enumerate(header.map):
            dataDict[key]  = a[:, i]
        return dataDict
   
            
class SpeedSelectorWidget(QtGui.QWidget):
    """This widget implements a series of buttons
    with which you can set the speed of playback."""
    SPEEDS = [ ("1/%d x"%d,1/d) for d in [8,4,2] ] + \
             [ ("%d x"%d,d) for d in [1,2,4,8] ] 
             
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self._hbox = QtGui.QHBoxLayout()
        
        self._buttonGroup = QtGui.QButtonGroup()
        self._buttons = []
        for s in self.SPEEDS:
            b = QtGui.QPushButton(s[0])
            b.setCheckable(True)
            b.speed = s[1]
            self._buttons.append(b)
            self._buttonGroup.addButton(b)
            self._hbox.addWidget(b)
            
        self.setLayout(self._hbox)
        self.setMinimumWidth(300)
        
        # Connect buttonGroup callback
        self._buttonGroup.buttonClicked.connect(self.ButtonPressed)

        # Set current speed to 1x
        self._buttons[3].setChecked(True)
        self._currentSpeed = 1;
        
    def getCurrentSpeed(self):
        return self._currentSpeed
        
    def ButtonPressed(self, buttonHandle):
        self._currentSpeed = buttonHandle.speed
        
class Group(QtGui.QGroupBox):
    """QGroupbox with init-function which allows easy setup."""
    def __init__(self,title,orientation,Children):
        QtGui.QGroupBox.__init__(self,title)
        if orientation == 'vertical':
            self._box = QtGui.QVBoxLayout()
        elif orientation == 'horizontal':
            self._box = QtGui.QHBoxLayout()
        else:
            raise ValueError("Unknown value for orientation (should be 'horizontal' or 'vertical', but was '%s')."%orientation)
        for b in Children:
            if isinstance(b, QtGui.QWidget):
                self._box.addWidget(b)
            elif isinstance(b, QtGui.QLayout):
                self._box.addLayout(b)
            else:
                raise ValueError("Unknown value for one of the children")
        self.setLayout(self._box)
        
##        
class TrajectoryWidget(QtGui.QWidget):
    def __init__(self, GetPositionValuesFcn, SetSpringValuesFcn, includeAnkles):
        QtGui.QWidget.__init__(self)
        
        self._includeAnkles = includeAnkles
        self._nDofs = 10 if self._includeAnkles else 8
        self._GetPositionValuesFcn = GetPositionValuesFcn
        self._SetSpringValuesFcn = SetSpringValuesFcn
        
        self.Config()
        self.BuildGUI()
        
        self._isRunning = False
        self._Trajectory = None
        self._time = 0
        self._currentStiffness = self._nDofs * [0]
        self._StiffnessSlider.setValue(0.5)
        
        self.StiffnessSliderMoved() # Updates the StiffnessSliderIndicator
        self.UpdateGUI()
        # Timer to update joint position lines
        self._T = QtCore.QTimer()
        self._T.timeout.connect(self.Update)
        self._T.start(1000/self._fps)
        
    def Config(self):
        """Sets configuration variables"""
        if self._includeAnkles:
            self._MaxStiffness = [100000, 100000] + 8*[10000]
        else:
            self._MaxStiffness = [100000, 100000] + 6*[10000]
        self._fps = 20
    
    def BuildGUI(self):
        def c(widget, callback):
            """adds the callback to the widget and returns a reference to the widget."""
            if isinstance(widget, QtGui.QPushButton):
                widget.clicked.connect(callback)
            elif isinstance(widget, QtGui.QSlider):
                widget.valueChanged.connect(callback)
            else:
                raise TypeError("subfunction c() cannot handle widgets of type %s."%widget)
            return widget
            
        self._ReadFileButton = c(QtGui.QPushButton("Read trajectory from file"), self.ReadFileButtonClicked)
        self._SineTrajectoryButton = c(QtGui.QPushButton("Sine trajectory (for debug)"), self.SineTrajectoryButtonClicked)
        self._FileIndicator = QtGui.QLabel("")
        self._LoopCheckbox = QtGui.QCheckBox("Loop")
        self._StartButton = c(QtGui.QPushButton("Start"),self.StartButtonClicked)
        self._StopButton = c(QtGui.QPushButton("Stop"), self.StopButtonClicked)
        self._SpeedSelector = SpeedSelectorWidget()
        self._TimeSlider = c(FloatSlider(QtCore.Qt.Orientation.Horizontal),self.TimeSliderMoved)
        self._TimeIndicator = QtGui.QLabel("- / -")
        self._StiffnessSlider = c(FloatSlider(QtCore.Qt.Orientation.Horizontal), self.StiffnessSliderMoved)
        self._StiffnessSlider.setMaximum(1.0)
        self._StiffnessIndicator = QtGui.QLabel("-")
        
        
        self._FileRow = QtGui.QHBoxLayout()
        self._FileRow.addWidget(self._ReadFileButton)
        self._FileRow.addWidget(self._SineTrajectoryButton)
        
        self._StartStopRow = QtGui.QHBoxLayout()
        self._StartStopRow.addWidget(self._StopButton)
        self._StartStopRow.addWidget(self._StartButton)
        
        
        self._vbox = QtGui.QVBoxLayout()
        self._vbox.addWidget(Group("Trajectory", 'vertical',[self._FileRow,self._FileIndicator]))
        self._vbox.addWidget(Group("Play Control", 'vertical', [self._LoopCheckbox, self._StartStopRow, self._SpeedSelector]))
        self._vbox.addWidget(Group("Time",'vertical',[self._TimeSlider, self._TimeIndicator]))
        self._vbox.addWidget(Group("Stiffness",'vertical',[self._StiffnessSlider, self._StiffnessIndicator]))
        self._vbox.addStretch()
        self.setLayout(self._vbox)
        
    def ReadFileButtonClicked(self):
        fileName = QtGui.QFileDialog.getOpenFileName(self,"Open Trajectory file", ".", "Supported files (*.csv *.signals);;All Files (*.*)")[0]
        if fileName=='':
            return
        extension = os.path.splitext(fileName)[1].lower()
        # If we're here, we can try to read the file
        try:
            if extension=='.signals':
                self._Trajectory = TrajectoryFromSignalsFile(self._includeAnkles,fileName)
            else:
                self._Trajectory = TrajectoryFromFile(self._includeAnkles,fileName)
        except Exception as e:
#             import pdb; pdb.set_trace()
            QtGui.QMessageBox.warning(self, "Error", "Could not read file. Error message from reader:\n%s"%e)
            return
        self._FileIndicator.setText(fileName)
        
        # Set the controls to not-running.
        self._time = 0
        self._isRunning = False
        
        self.UpdateGUI()
    def SineTrajectoryButtonClicked(self):
        self._Trajectory=SineTrajectory(self._includeAnkles,self._nDofs*[0.05])
        self._FileIndicator.setText('Sine trajectory')
        self.UpdateGUI()
        
    def StartButtonClicked(self):
        self._isRunning = True
        self.UpdateGUI()
        self.UpdateGUIAnimation()
        
    def StopButtonClicked(self):
        self._isRunning = False
        self.Update(force=True, updateTime = False)
        self.UpdateGUI()
        self.UpdateGUIAnimation()
        
    def TimeSliderMoved(self):
        if not self._isRunning:
            self._time = self._TimeSlider.value()
            self.Update(force=True, updateTime = False)
        
    def StiffnessSliderMoved(self):
        pct_nonlin = self._StiffnessSlider.value()**3
        self._currentStiffness = [ round(mx * pct_nonlin) for mx in self._MaxStiffness ]
        self._StiffnessIndicator.setText('[ '+','.join( ['%d'%int(s) for s in self._currentStiffness])+' ]')
        if not self._isRunning:
            self.Update(force=True, updateTime = False)
        
    def UpdateGUI(self):
        if self._Trajectory is None:
            self._StopButton.setEnabled(False)
            self._StartButton.setEnabled(False)
            self._TimeSlider.setEnabled(False)
            self._TimeSlider.setMaximum(1)
        else:
            # There is a trajectory
            self._StopButton.setEnabled(self._isRunning)
            self._StartButton.setEnabled(not self._isRunning)
            self._TimeSlider.setEnabled(not self._isRunning)
            self._TimeSlider.setMaximum(self._Trajectory.getDuration())
        self.UpdateGUIAnimation()
            
    def UpdateGUIAnimation(self):
        """Updates all elements of the GUI that change during playing."""
        if self._Trajectory is None:
            self._TimeIndicator.setText("- / -")
            self._TimeSlider.setValue(0)
        else:
            self._TimeIndicator.setText("%.3f / %.3f"%(self._time, self._Trajectory.getDuration()))
            self._TimeSlider.setValue(self._time)
            
    def _SetSpringValuesWrapper(self,p,s):
        """This class assumes that, if _includeAnkles is false, everything is
        8 dof. However, setSpringValues should always be called with 10 elements.
        This wrapper does the conversion."""
        if self._includeAnkles:
            # Then simply pass it on
            self._SetSpringValuesFcn(p,s)
        else:
            self._SetSpringValuesFcn(p[0:5]+[0]+p[5:8]+[0],s[0:5]+[0]+s[5:8]+[0])

    def Update(self, force=False, updateTime = True):
        if self._Trajectory is not None and (self._isRunning or force):
            if updateTime:
                self._time += self._SpeedSelector.getCurrentSpeed() * 1/self._fps
                if self._time > self._Trajectory.getDuration():
                    if self._LoopCheckbox.isChecked():
                        self._time %= self._Trajectory.getDuration()
                    else:
                        self._isRunning = False
                        self._time = 0
                        self.UpdateGUI()
                        
            if self._isRunning:
                values = self._Trajectory.values(self._time)
                self._SetSpringValuesWrapper(values,self._currentStiffness)
            else:
                self._SetSpringValuesWrapper(self._nDofs*[0],self._nDofs*[0])
                        
            self.UpdateGUIAnimation()
        
## TEST SCRIPT
if __name__=='__main__':
    u=10*[0.5]        
    def x():
        return u      
        
    def Send(pos,sti):
        print("set spring pos "+pos.__str__()+";set spring stiffness "+sti.__str__())
        
    F = TrajectoryWidget(None,Send,includeAnkles=False )
#     F.setGeometry(QtCore.QRect(2094, 211, 610,610))
    F.show()
    