## ForceInteractionWidget.py
from PySide import QtCore, QtGui
from PerReader import PerReader
import random

def limit(a,minn,maxx):
    return max([ min([a,maxx]),minn])
    
class SingleSpringWidget(QtGui.QWidget):
    """Widget which shows a square, and has the functionality of emitting a signal
    containing position and stiffness for the specific joint. Each joint should
    have one such widget."""
    SpringChanged = QtCore.Signal(float,float) # Arguments: position, stiffness
    
    COLOR_OFF  = QtGui.QColor(0xaaaaaa)
    COLOR_ON   = QtGui.QColor(0x88ff88)
    COLOR_PERM = QtGui.QColor(0x00ff00) # When a force is permanently on
    COLOR_CROSS=QtGui.QColor(0xaa1111)
    COLOR_VERTLINE=QtGui.QColor(0x1111aa)
    COLOR_ZERO = QtGui.QColor(0x444444)
    
    class SpringRectangle(QtGui.QWidget):
        """The actual rectangle that is used to get user input for the position
        and spring stiffness. On mouse action, we directly emit the data to the
        parent class so that that can handle everything."""
        MouseEvent=QtCore.Signal(int, list) # type, (x_norm, y_norm) (type: 1: mousepress; 2:mouserelease; 3: move, btn down; 4: move, btn up; 5: doubleclick. 0<=x_norm,y_norm<=1)
        
        def __init__(self, parent):
            QtGui.QWidget.__init__(self)
            self._parent = parent
            self.setMouseTracking(True)
            self._crossPos = None
            self._vertLinePos = None
            self._zeroLinePos = None
            self.show()
            
        def SetBackgroundColor(self,color):
            """Color should be a QtGui.QColor (or, a predefined from QtCore.Qt (e.g. QtCore.Qt.red)"""
            self.setAutoFillBackground(True)
            p = self.palette()
            p.setColor(self.backgroundRole(), color)
            self.setPalette(p)
            
        def SetCrossPosition(self,pos_norm):
            """Use this function to make the cross appear on a certain spot.
            Set pos_norm to None to remove it.
            (The cross indicates the current position and stiffness of the spring)"""
            self._crossPos = pos_norm
            self.update() # Make Qt repaint the window
        
        def SetVertLinePosition(self,pos_norm):
            """Use this function to make the vertical line appear on a certain spot.
            Set pos_norm to None to remove it.
            (The line indicates the current joint position)"""
            self._vertLinePos = pos_norm
            self.update()
            
        def SetZeroLinePosition(self,pos_norm):
            """Use this function to make a vertical line appear at some 'zero' value."""
            self._zeroLinePos = pos_norm
            self.update()
            
        def getNormalizedCoordinates(self,pos):
            """Returns the coordinates in a normalized way. They're also limited to be 0<=x,y<=1.
            a should be of type QtCore.QPoint."""
            return limit(pos.x()/self.width(),0,1), limit(1-pos.y()/self.height(),0,1)
            
        def mousePressEvent(self,event):
            if event.buttons() & QtCore.Qt.LeftButton: # If left button is pressed
                self.MouseEvent.emit(1, self.getNormalizedCoordinates(event.pos()))
        def mouseReleaseEvent(self,event):
            self.MouseEvent.emit(2, self.getNormalizedCoordinates(event.pos()))
        def mouseMoveEvent(self,event):
            if event.buttons() & QtCore.Qt.LeftButton: # If left button is pressed
                self.MouseEvent.emit(3, self.getNormalizedCoordinates(event.pos()))
            else:
                self.MouseEvent.emit(4, self.getNormalizedCoordinates(event.pos()))
        def mouseDoubleClickEvent(self,event):
            if event.buttons() & QtCore.Qt.LeftButton: # If left button is pressed
                self.MouseEvent.emit(5, self.getNormalizedCoordinates(event.pos()))
        
        def paintEvent(self,event):
            CROSS_HALFSIZE=8
            
            qp = QtGui.QPainter()
            qp.begin(self)
            if self._zeroLinePos is not None:
                x = self._zeroLinePos * self.width()
                qp.setPen(QtGui.QPen(self._parent.COLOR_ZERO,1,QtCore.Qt.DashLine))
                qp.drawLine(QtCore.QLineF( x,0,x,self.height()))
   
            if self._vertLinePos is not None:
                x = self._vertLinePos*self.width()
                qp.setPen(QtGui.QPen(self._parent.COLOR_VERTLINE, 3, QtCore.Qt.SolidLine))
                qp.drawLine(QtCore.QLineF( x,0,x,self.height()))

            if self._crossPos is not None:
                x,y = self._crossPos[0]*self.width(), (1-self._crossPos[1])*self.height()
                qp.setPen(QtGui.QPen(self._parent.COLOR_CROSS, 3, QtCore.Qt.SolidLine))
                qp.drawLine(QtCore.QLineF( x-CROSS_HALFSIZE,y, x+CROSS_HALFSIZE,y))
                qp.drawLine(QtCore.QLineF( x,y-CROSS_HALFSIZE, x,y+CROSS_HALFSIZE))
            qp.end()
            
    def __init__(self, name, positionRange, stiffnessRange):
        QtGui.QWidget.__init__(self)
        
        self._name = name
        self._positionRange = positionRange
        self._stiffnessRange = stiffnessRange
        self._vbox = QtGui.QVBoxLayout()
        
        self._rect = self.SpringRectangle(self)
        self._rect.SetBackgroundColor(self.COLOR_OFF)
        self._label = QtGui.QLabel(self._name)
        self._label.setStyleSheet('font-size:14pt')
        self._label.setTextFormat(QtCore.Qt.RichText)
        self._vbox.addWidget(self._rect)
        self._vbox.addWidget(self._label)
        self.setLayout(self._vbox)
        self._vbox.setStretch(0,1) # Make rect as large as possible; keep label small (automatically gets large enough to contain all text)
        
        self._vbox.setContentsMargins(0,0,0,0)
        self._vbox.setSpacing(1)
        
        # 'state':
        self._SpringPermanentlyOn = False
        self._SpringOn = False
        self._position = 0
        self._stiffness = 0
        self._linePosition = None
        
        self._rect.MouseEvent.connect(self.handleRectangleMouseEvent)
        
    def SetLabelText(self):
        s = self._name
        if self._linePosition is not None:
            s=s+' <font color="%s">(%.3f)</font>'%(self.COLOR_VERTLINE.name(),self._linePosition)
        if self._SpringOn:
            s=s+' <font color="%s">->(%.3f, %.0f)</font>'%(self.COLOR_CROSS.name(),self._position,self._stiffness)
        self._label.setText(s)
        
    def CalculateSpringParamsAndEmit(self, normPos, on):
        self._position  = self._positionRange[0] + normPos[0] * (self._positionRange[1]-self._positionRange[0])
        if on:
            self._stiffness = self._stiffnessRange[0] + (normPos[1]**3) * (self._stiffnessRange[1]-self._stiffnessRange[0])
        else:
            self._stiffness = 0
        self.SpringChanged.emit(self._position, self._stiffness)
        
    def handleRectangleMouseEvent(self,type, normPos):
        """Handles (indirectly) a mouse event from the SpringRectangle.
        Type: 1: mousepress; 2:mouserelease; 3: move; 4: doubleclick."""
        if type==1 or type==3: # Press, move btn down
            self._SpringOn = True
            self.CalculateSpringParamsAndEmit(normPos, True)
            self._rect.SetCrossPosition(normPos)
        elif type==2: # Release
            if self._SpringPermanentlyOn:
                self._SpringOn = True
                self.CalculateSpringParamsAndEmit(normPos, True)
                self._rect.SetCrossPosition(normPos)
            else:
                self._SpringOn = False
                self.CalculateSpringParamsAndEmit(normPos, False)
                self._rect.SetCrossPosition(None)
        elif type==4:
            # print("Movement without button. (%f,%f)"%(normPos[0],normPos[1]))
            pass
        elif type==5: # Doubleclick
            self._SpringPermanentlyOn = not self._SpringPermanentlyOn
            self._SpringOn = True
            self.CalculateSpringParamsAndEmit(normPos, True) # True, because at this time the mouse button is pressed, so we consider that as to apply the spring
            self._rect.SetCrossPosition(normPos)
        else:
            raise ValueError("Wrong value for type (got %d; expected 1...5)"%type)
            
        if self._SpringPermanentlyOn:
            self._rect.SetBackgroundColor(self.COLOR_PERM)
        elif type in [1,3,5]:
            self._rect.SetBackgroundColor(self.COLOR_ON)
        else:
            self._rect.SetBackgroundColor(self.COLOR_OFF)
        self.Update()
        
    def SetJointPositionLine(self, position):
        """Given position [m or rad], it draws a blue vertical line
        at the given pos. if position is None, it removes the line."""
        self._linePosition = position
        if position is None:
            self._rect.SetVertLinePosition(None)
        else:
            minn,maxx=self._positionRange
            self._rect.SetVertLinePosition ( (position-minn)/(maxx-minn))
        self.Update()
        
    def SetPositionRange(self,range):
        self._positionRange = range
        self._rect.SetZeroLinePosition( (0-range[0])/(range[1]-range[0]))
        self.Update()
            
    def Update(self):
        """Updates the whole appearance of the widget."""
        self.SetLabelText()
        
class ForceInteractionWidget(QtGui.QWidget):
    """A widget that shows fields which can be used for force interaction with the model.
    The user should provide functions 
    - GetPositionValuesFcn() 
      returning a list of 10 floats returning the actual positions of the 10 joints,
    - SetSpringValuesFcn (SpringPositions, SpringStiffnesses)
      which sends the values to the LLC.
    """
    order=['PX','PZ','LHA','LHF','LKF','LAP','RHA','RHF','RKF','RAP']
    
    def __init__(self, GetPositionValuesFcn, SetSpringValuesFcn, includeAnkles):
        QtGui.QWidget.__init__(self)
        
        self._includeAnkles = includeAnkles
        self._GetPositionValuesFcn = GetPositionValuesFcn
        self._SetSpringValuesFcn = SetSpringValuesFcn
        
        # First we need to make a grid of all widgets
        self.GridLayout = QtGui.QGridLayout()
        self.setLayout(self.GridLayout)
        self.GridLayout.setContentsMargins(3,3,3,3)
        self.GridLayout.setSpacing(10)
        
        # Now, add all springWidgets. Position limits are set later.
        px  = SingleSpringWidget('PX',[0,1],[0,100000])
        pz  = SingleSpringWidget('PZ',[0,1],[0,100000])
        lha = SingleSpringWidget('LHA',[0,1],[0,10000])
        lhf = SingleSpringWidget('LHF',[0,1],[0,10000])
        lkf = SingleSpringWidget('LKF',[0,1],[0,10000])
        lap = SingleSpringWidget('LAP',[0,1],[0,10000])
        rha = SingleSpringWidget('RHA',[0,1],[0,10000])
        rhf = SingleSpringWidget('RHF',[0,1],[0,10000])
        rkf = SingleSpringWidget('RKF',[0,1],[0,10000])
        rap = SingleSpringWidget('RAP',[0,1],[0,10000])
        self._springWidgets = {'PX':px, 'PZ':pz, \
            'LHA':lha, 'LHF':lhf, 'LKF':lkf, 'LAP':lap, \
            'RHA':rha, 'RHF':rhf, 'RKF':rkf, 'RAP':rap }
        self.GridLayout.addWidget(px,  0,0)
        self.GridLayout.addWidget(pz,  0,1)
        self.GridLayout.addWidget(lha, 1,0)
        self.GridLayout.addWidget(lhf, 2,0)
        self.GridLayout.addWidget(lkf, 3,0)
        self.GridLayout.addWidget(rha, 1,1)
        self.GridLayout.addWidget(rhf, 2,1)
        self.GridLayout.addWidget(rkf, 3,1)
        if self._includeAnkles:
            self.GridLayout.addWidget(lap, 4,0)
            self.GridLayout.addWidget(rap, 4,1)
            
        for p in self._springWidgets.values():
            p.SpringChanged.connect(self.SpringChangedHandle)
            
        self.ApplyRangesFromXMLFile()
            
        # Timer to update joint position lines
        self._T = QtCore.QTimer()
        self._T.timeout.connect(self.UpdatePositionLines)
        self._T.start(50); # 20 fps
        
    def UpdatePositionLines(self):
        """Called once in a while to update the positions of the joint position indicator lines."""
        if self._GetPositionValuesFcn is not None:
            p = self._GetPositionValuesFcn()
        else:
            p = [random.random() for x in range (10)]
        for i in range(10):
            self._springWidgets[self.order[i]].SetJointPositionLine(p[i])
        
    def SpringChangedHandle(self):
        """Connected to signals that indicate that the spring stiffness of any of
        the joints was changed. This gathers all information and calls the setter function."""
        
        allPositions =  [self._springWidgets[x]._position for x in self.order]
        allStiffnesses =[self._springWidgets[x]._stiffness for x in self.order]
        if self._SetSpringValuesFcn is not None:
            self._SetSpringValuesFcn(allPositions,allStiffnesses)
        else:
            print("set spring pos "+allPositions.__str__()+";set spring stiffness "+allStiffnesses.__str__())
        
    def ApplyRangesFromXMLFile(self,xmlFile=None):
        """Reads joint ranges from the given xml file (should point to a PER.XML),
        and applies them to all fields. If xmlFile is not given, it tries to read from
        the default xml file."""
        if xmlFile is not None:
            P = PerReader(xmlFile)
        else:
            P = PerReader()
        
        # Some of the ranges should be negated because the xml file 
        for k,o in self._springWidgets.items():
            o.SetPositionRange(P.GetJointRangeAnatomic(k))
        
## Test script        
if __name__=='__main__':
    # Test script
    u=10*[0.5]        
    def x():
        return u      
        
    def Send(pos,sti):
        print("set spring pos "+pos.__str__()+";set spring stiffness "+sti.__str__())
        
    F = ForceInteractionWidget(None,Send,includeAnkles=False )
    F.setGeometry(QtCore.QRect(2094, 211, 610,610))
    F.show()
            
    ww=F._springWidgets['PX']